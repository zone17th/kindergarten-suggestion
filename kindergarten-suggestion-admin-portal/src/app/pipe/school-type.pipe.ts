import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'schoolType',
  standalone: true
})
export class SchoolTypePipe implements PipeTransform {

  transform(value: string): string {
    if (value === 'SEMI_PUBLIC') {
      return 'Semi-public';
    }
    return value
      .toLowerCase()
      .replace(/_/g, ' ')
      .replace(/\b\w/g, firstChar => firstChar.toLocaleUpperCase());
  }
}
