import { ComponentFixture, TestBed } from '@angular/core/testing';
import {ViewCrudSchoolComponent} from "./view-crud-school.component";

describe('ViewCRUDSchoolComponent', () => {
  let component: ViewCrudSchoolComponent;
  let fixture: ComponentFixture<ViewCrudSchoolComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ViewCrudSchoolComponent]
    });
    fixture = TestBed.createComponent(ViewCrudSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
