import {Component, OnInit} from '@angular/core';
import {en_US, NzI18nService} from "ng-zorro-antd/i18n";
import {SchoolService} from "../../../../../service/school-service/school.service";
import {first} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-ratings-feedbacks',
  templateUrl: './ratings-feedbacks.component.html',
  styleUrls: ['./ratings-feedbacks.component.scss']

})
export class RatingsFeedbacksComponent implements OnInit {
  DATE_FORMAT_JSON: string = 'yyyy-MM-dd';
  date1!: Date;
  date2!: Date;
  ratingGroup!: any;
  date = null;
  id!: number;
  schoolRatings!: any;
  feedbacks: any[] = [];
  feedbackActivePage = 0;
  feedbackTotalPage = 0;
  user!: any;
  avgRating!: any;
  keys!: any;
  feedbackCount!: any;
  size: any = 3;
  start!: string;
  end!: string;
  initLoading = true; // bug

  constructor(private i18n: NzI18nService,
              private _schoolService: SchoolService,
              private _activatedRoute: ActivatedRoute,
              private _dataPipe: DatePipe,
              private _router: Router) {
    i18n
      .setLocale(en_US);
  }

  ngOnInit() {
    this._activatedRoute.queryParams.pipe(first()).subscribe(params => {
      const ratingParam = params['rating'];
      this.ratingGroup = ratingParam ? (Array.isArray(ratingParam) ? ratingParam : [ratingParam]) : [];
    });

    this.date1 = new Date();
    this.date2 = new Date();
    this._activatedRoute.params.subscribe(params => {
      this.id = Number(params['id']);
      this.start = params['start']
      this.end = params['end'];

    });
    this.loadRatingsSchool();
    this.loadSchoolFeedbacks();
  }

  onLoadMore() {
    this.feedbackActivePage++;
    this.loadSchoolFeedbacks();
  }

  loadRatingsSchool() {
    return this._schoolService.getAllRatingsSchool(this.id)
      .subscribe({
        next: resp => {
          this.schoolRatings = {
            learningProgram: resp.avgLearningProgramRating,
            facilitiesAndUtilities: resp.avgFacilityAndUtilityRating,
            extracurricularActivities: resp.avgExtracurricularActivityRating,
            teachersAndStaff: resp.avgTeacherAndStaffRating,
            hygieneAndNutrition: resp.avgHygieneAndNutritionRating,
          };
          this.avgRating = resp.avgRating;
          this.feedbackCount = resp.feedbackCount;
          this.keys = Object.keys(this.schoolRatings);
        }, error: err => {
          console.error(err)
        }
      })
  }

  loadSchoolFeedbacks() {
    this._activatedRoute.queryParams.subscribe(params => {
      this._schoolService.getAllFeedBacksSchool(this.id, this.feedbackActivePage, params).subscribe({
        next: res => {
          console.log(res.content)
          this.feedbacks.push(...res.content);
          this.feedbackActivePage = res.pageable?.pageNumber;
          this.feedbackTotalPage = res.totalPages;
          this.initLoading = false;
        }
      })
    });
  }

  updateUrl() {
    const queryParams = {
      start: this._dataPipe.transform(this.date1, this.DATE_FORMAT_JSON),
      end: this._dataPipe.transform(this.date2, this.DATE_FORMAT_JSON)
    };
    this._router.navigate([], {
      queryParams,
      queryParamsHandling: 'merge'
    });
  }

  onChange1(result1: Date): void {
    this.date1 = result1;
    if (this.date1 > this.date2) {
      this.date2 = new Date();
    }
    this.updateUrl();
  }

  onChange2(result2: Date): void {
    this.date2 = result2;
    if (this.date2 < this.date1) {
      this.date1 = this.date2
    }
    this.updateUrl()
  }


  refresh() {
    this.date1 = new Date();
    this.date2 = new Date();
  }

  disabledDate = (current: Date): boolean => {
    // Lấy ngày hiện tại
    const currentDate = new Date();
    // So sánh ngày hiện tại với ngày được chọn
    return current.getTime() > currentDate.getTime();
  }

  buildUrlCondition(event: any, fieldName: string) {
    console.log(event);
    if (fieldName === 'rating') {
      this.ratingGroup = event;
      const queryParams = {
        rating: this.ratingGroup
      };
      this._router.navigate([], {
        queryParams,
        queryParamsHandling: "merge",
      });
    }
  }

}
