import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { Subject, takeUntil } from "rxjs";
import { UserService } from "../../../../service/user-service/user.service";

@Component({
  selector: 'app-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.scss']
})
export class ViewListComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();
  userList: any[] = [];
  searchOptions: string = '';
  dataLoaded = false;
  successMessage: string = "Successfully deleted!";
  failMessage: string = "Failed!";
  message!: string;
  errorMessage!: string;
  page = 0;
  size = 10;
  totalElements = 0;



  constructor(private _userService: UserService,
              private _router: Router,
              private _activeRoute: ActivatedRoute,
              private _notification: NzNotificationService) {
  }


  ngOnInit(): void {
    this._activeRoute.queryParams.subscribe(params => {
      this.page = Number(params['page']) || 0;
      this.searchOptions = params['query'];
    });
    this.loadData();
  }

  search() {
    this.reGenerateUrl();
    this.loadData();
  }

  loadData() {
    this._userService.getUserList(this.page, this.size, this.searchOptions)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: res => {
        this.userList = res.content;
        this.totalElements = res.totalElements;
        this.dataLoaded = true;
      },
      error: error => {

      }
    });
  }

  delete(id: number): void {
    this._userService.delete(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: res => {
          this.createNotification('success', this.successMessage, this.message);
          this.loadData();
        },
        error: error => {
          this.errorMessage = error.error;
          this.message = 'Please try again!'
          this.createNotification('error', this.failMessage, this.message);
        }
      });
  }

  createNotification(type: string, title: string, message: string): void {
    this._notification.create(
      type,
      title, message
    );
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  paging(newPage: number) {
    this.page = newPage - 1;
    this.reGenerateUrl();
    this.loadData();
  }

  reGenerateUrl() {
    const navigationExtras: NavigationExtras = {
      queryParams: {query: this.searchOptions, page: this.page, size: this.size}
    };
    this._router.navigate([], navigationExtras);
  }

}
