import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {ReactiveFormsModule} from '@angular/forms';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzInputModule} from 'ng-zorro-antd/input';
import {AdminRoutingModule} from './admin-routing.module';
import {AdminLoginComponent} from "../auth/auth-login/admin-login.component";
import {ForgotPasswordComponent} from "../auth/auth-forgot-password/forgot-password.component";
import {ResetPasswordComponent} from "../auth/auth-reset-password/reset-password.component";

@NgModule({
  declarations: [

    AdminLoginComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,

  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    ReactiveFormsModule
  ]
})
export class AdminModule {
}
