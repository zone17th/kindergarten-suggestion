import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ParentListComponent } from "./parent-list/parent-list.component";
import {ParentDetailComponent} from "./parent-detail/parent-detail/parent-detail.component";
import {SchoolListEnrollComponent} from "./school-list-enroll/school-list-enroll.component";


const routes: Routes = [
  { path: '', component: ParentListComponent },
  { path: 'list', component: ParentListComponent },
  { path: 'detail/:id', component: ParentDetailComponent},
  { path: 'enrolls/:id', component: SchoolListEnrollComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParentRoutingModule { }
