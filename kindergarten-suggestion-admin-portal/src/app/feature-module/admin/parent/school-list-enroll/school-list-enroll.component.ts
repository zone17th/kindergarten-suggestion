import {Component, OnInit} from '@angular/core';
import {ParentService} from "../../../../service/parent-service/parent-service.service";
import {Subject, takeUntil} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {NzNotificationService} from "ng-zorro-antd/notification";

@Component({
  selector: 'app-school-list-enroll',
  templateUrl: './school-list-enroll.component.html',
  styleUrls: ['./school-list-enroll.component.scss']
})
export class SchoolListEnrollComponent implements OnInit{
  private unsubscribe$: Subject<void> = new Subject<void>();
  schoolEnroll: any [] = [];
  parentID = 0;
  isVisible = false;
  successMessage: string = "Successfully unEnroll!";
  failMessage: string = "Failed!";
  message!: string;
  errorMessage!: string;

  constructor(private _parentService: ParentService,
              private _activeRoute: ActivatedRoute,
              private _notification: NzNotificationService) {
  }
  ngOnInit(): void {
    this._activeRoute.paramMap.subscribe(params => {
      const idString = params.get('id');
      if (idString) {
        this.parentID = parseInt(idString);
      }
    })
    this.loadData();

  }
  loadData() {
    this._parentService.getSchoolList(this.parentID).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: res => {
          this.schoolEnroll = res;
          this.schoolEnroll.forEach(e=>e['isVisible']=this.isVisible);
          console.log(this.schoolEnroll);
        },
        error: error => {

        }
      });
  }

  showModal(data:any) {
    data.isVisible = true;
    console.log(data)
  }

  handleCancel(data:any) {
    data.isVisible = false;
  }
  handleOk(data:any) {
    data.isVisible = false;
  }

  delete(id: number): void {
    this._parentService.updateUnEnrollParent(this.parentID, id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: res => {
          this.createNotification('success', this.successMessage, this.message);
          this.loadData();
        },
        error: error => {
          this.errorMessage = error.error;
          this.message = 'Please try again!'
          this.createNotification('error', this.failMessage, this.message);
        }
      });
  }
  createNotification(type: string, title: string, message: string): void {
    this._notification.create(
      type,
      title, message
    );
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
