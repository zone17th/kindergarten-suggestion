import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { MailService } from 'src/app/service/mail-service/mail.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  validateForm!: UntypedFormGroup;
  inputEmailTouched: boolean = false;
  
  constructor(private fb: UntypedFormBuilder,
              private _mailService: MailService,
              private _router: Router,
              private notification: NzNotificationService) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [null, [Validators.required,Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
      fullName: null,
    });
     // Theo dõi sự kiện focus của ô input
     this.email?.valueChanges.subscribe(() => {
      this.inputEmailTouched = true;
    });
  }
  
  submitForm(): void {
    this.inputEmailTouched = true;
    if (this.validateForm.valid) {
      console.log('submit', this.validateForm.value);
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
    console.log(this.validateForm.value);
    this._mailService.forgotPassword(this.validateForm.value).subscribe({
      next: response => {
        this.createNotification('success','Send email success','We have sent an email with the link to reset your password.');
        setTimeout(() => this.homePage(),2000);
      },
      error: error =>{
        this.createNotification('error','Send email failed','The email address doesn’t exist. Please try again!');
      }
    })
  }
  homePage(): void {
    this._router.navigate(['/admin/auth/login']);
  }
  createNotification(type: string,title:string,content:string): void {
    this.notification.create(type,title,content);
  } 
  get email() { return this.validateForm.get('email'); }
}
