import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { NgModule } from '@angular/core';
import { SchoolModule } from "./feature-module/admin/school/school.module";
import { LayoutModule } from './layout/layout.module';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterOutlet } from '@angular/router';

import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzFormModule } from "ng-zorro-antd/form";
import { NZ_I18N, vi_VN } from 'ng-zorro-antd/i18n';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzTableModule } from 'ng-zorro-antd/table';
import { HTTP_INTERCEPTOR_PROVIDERS } from "./interceptor";
import { RequestPipePipe } from './pipe/request-pipe.pipe';
import {DatePipe} from "@angular/common";


@NgModule({
  declarations: [
    AppComponent,
    RequestPipePipe,
    // EducationDisplayPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterOutlet,
    NzLayoutModule,
    NzBreadCrumbModule,
    NzModalModule,
    NzNotificationModule,
    NzButtonModule,
    SchoolModule,
    NzFormModule,
    NzBreadCrumbModule,
    CKEditorModule,
    NzTableModule
  ],
  providers: [
    {provide: NZ_I18N, useValue: vi_VN},
    HTTP_INTERCEPTOR_PROVIDERS,
    DatePipe
  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
