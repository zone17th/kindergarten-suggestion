import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {

  constructor(private _httpClient: HttpClient) { }
  adminLogin(authPayload: {email: string, password: string}): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/admin/login`, authPayload);
  }
  logOut(): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/auth/logout`,{});
  }
  resetPassword(formData: any): Observable<any> {
    return this._httpClient.put(`${environment.apiPrefixUrl}/account/reset-password`, formData);
  }
  loadResetPage(token:string): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/account/reset-password/${token}`,{responseType:'text'});
  }
}
