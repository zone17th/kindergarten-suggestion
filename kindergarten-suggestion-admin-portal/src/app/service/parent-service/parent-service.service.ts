import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class ParentService {

  constructor(private _httpClient: HttpClient) { }

  getParentList(page: number, size: number, searchOptions: any): Observable<any> {
    let params = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString());
    if (searchOptions) {
      params = params.set('q', searchOptions);
    }
    return this._httpClient.get(`${environment.apiPrefixUrl}/admin/parent/list`, {
      params
    });
  }
  getParentDetailById(id: number): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/admin/parent/${id}`);
  }

  updateUnEnrollParent(id: number, schoolID: number): Observable<any> {
    return this._httpClient
      .put(`${environment.apiPrefixUrl}/admin/parent/unEnroll/${id}/${schoolID}`, {});
  }

  enrollParent(id: number, schoolID: number): Observable<any> {
    return this._httpClient
      .post(`${environment.apiPrefixUrl}/admin/parent/enroll/${id}/${schoolID}`, {});
  }
  getSchoolList(id: number): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/admin/parent/enroll/${id}`);
  }
}
