import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  constructor(private _httpClient: HttpClient) { }
  forgotPassword(authPayload: any): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/mail/password`, authPayload);
  }

  createNewUser(userInfo: any): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/mail/newUser`, userInfo);
  }
  schoolSubmit(status: string, id: number): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/mail/school-${status}/${id}`, { responseType: 'text' });
  }
}
