import { Injectable } from '@angular/core';
import jwtDecode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  accessToken: string | null = localStorage.getItem('accessToken');

  // Phương thức để lấy thông tin vai trò từ AccessToken
  getRolesFromAccessToken() {
    try {
      if (!this.accessToken) {
        return null;
      }
      const decodedToken = jwtDecode(this.accessToken);
      // 'roles' là key mà bạn đã sử dụng để lưu thông tin vai trò trong AccessToken
      return decodedToken;
      console.log(decodedToken);
    } catch (error) {
      console.error('Lỗi khi giải mã AccessToken:', error);
      return null;
    }
  }

}
