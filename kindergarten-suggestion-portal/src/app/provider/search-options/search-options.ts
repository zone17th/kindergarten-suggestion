import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class SearchOptions {
    q!: string;
    location!: number;
    district!: number;
    schoolType: string = '';
    age: string ='';
    minFee!: number;
    maxFee!: number;
    facilities: any[] = [];
    utilities: any[] = [];
    sort:number=1;
    page!: number;

}
