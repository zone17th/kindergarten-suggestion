import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { environment } from "../environment/environment";



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private _httpClient: HttpClient) { }
  login(authPayload: {email: string, password: string}): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/auth/login`, authPayload);
  }
  register(authPayload:{fullName:any;email:any;phoneNumber:any;password:any;confirmPassword:any}):Observable<any>{
    return this._httpClient.post(`${environment.apiPrefixUrl}/auth/register`,authPayload);
  }
  logOut(): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/auth/logout`,{});
  }
  accountAcctive(token:string): Observable<any> {
    return this._httpClient.put(`${environment.apiPrefixUrl}/account/active/${token}`,{responseType:'text'});
  }
}
