import { TestBed } from '@angular/core/testing';

import { RequestCounsellingService } from './request-counselling.service';

describe('RequestCounsellingService', () => {
  let service: RequestCounsellingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RequestCounsellingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
