import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangePasswordComponent } from "./change-password/change-password.component";
import { MyRequestComponent } from './my-request/my-request.component';
import { ViewAccountComponent } from "./view-account/view-account.component";

const routes: Routes = [
  {path: '', component: ViewAccountComponent},
  { path: 'profile', component: ViewAccountComponent },
  { path: 'password', component: ChangePasswordComponent },
  { path: 'request', component: MyRequestComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
