import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolEnrolledComponent } from './school-enrolled.component';

describe('SchoolEnrolledComponent', () => {
  let component: SchoolEnrolledComponent;
  let fixture: ComponentFixture<SchoolEnrolledComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SchoolEnrolledComponent]
    });
    fixture = TestBed.createComponent(SchoolEnrolledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
