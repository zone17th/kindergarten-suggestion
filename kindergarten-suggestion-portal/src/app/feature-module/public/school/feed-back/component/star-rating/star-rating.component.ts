import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: StarRatingComponent
    }
  ]
})
export class StarRatingComponent implements ControlValueAccessor {
  @Input() rating: number = 0;
  @Output() ratingChange = new EventEmitter<number>();
  onChange = (rating: number) => {
  };
  onTouched = () => {
  };

  touched = false;

  disabled = false;

  stars: number[] = [1, 2, 3, 4, 5];

  rate(rating: number) {
    this.rating = rating;
    this.onChange(this.rating);
    this.ratingChange.emit(this.rating);
  }

  writeValue(rating: number) {
    this.rating = rating;
  }

  registerOnChange(onChange: any) {
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }
}
