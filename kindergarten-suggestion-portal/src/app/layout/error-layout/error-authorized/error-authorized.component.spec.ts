import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorAuthorizedComponent } from './error-authorized.component';

describe('ErrorAuthorizedComponent', () => {
  let component: ErrorAuthorizedComponent;
  let fixture: ComponentFixture<ErrorAuthorizedComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorAuthorizedComponent]
    });
    fixture = TestBed.createComponent(ErrorAuthorizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
