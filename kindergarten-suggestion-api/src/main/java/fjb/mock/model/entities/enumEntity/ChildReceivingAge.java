package fjb.mock.model.entities.enumEntity;

import fjb.mock.model.appEnum.ChildReceivingAgeEnum;
import fjb.mock.model.entities.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
@Entity
public class ChildReceivingAge extends BaseEntity {
    @Enumerated(EnumType.STRING)
    private ChildReceivingAgeEnum code;
    private String name;
}
