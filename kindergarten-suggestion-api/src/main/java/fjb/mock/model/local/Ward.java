package fjb.mock.model.local;

import fjb.mock.model.entities.School;
import fjb.mock.model.entities.User;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Entity
@Data
public class Ward {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private District district;

    @OneToMany(mappedBy = "ward")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<User> user;

    @OneToMany(mappedBy = "ward")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<School> school;
}
