package fjb.mock.model.appEnum;

public enum UserRoleEnum {
    ADMIN, PARENTS, SCHOOL_OWNER
}
