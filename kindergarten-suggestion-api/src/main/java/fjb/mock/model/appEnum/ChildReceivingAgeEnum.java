package fjb.mock.model.appEnum;

import lombok.Getter;

@Getter
public enum ChildReceivingAgeEnum {
    AGE_6M_1Y("6 month - 1 year"),
    AGE_1Y_3Y("1 - 3 year"),
    AGE_3Y_6Y("3 - 6 year");

    public final String displayName;

    ChildReceivingAgeEnum(String displayName) {
        this.displayName = displayName;
    }
}
