package fjb.mock.model.dto.schoolDTO.adminSchoolDTO;

import fjb.mock.model.appEnum.ChildReceivingAgeEnum;
import fjb.mock.model.appEnum.EducationMethodEnum;
import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.appEnum.SchoolTypeEnum;
import fjb.mock.model.dto.addressDTO.DistrictDTO;
import fjb.mock.model.dto.addressDTO.ProvinceDTO;
import fjb.mock.model.dto.addressDTO.WardDTO;
import fjb.mock.model.dto.schoolDTO.SchoolFacilitiesResponseDTO;
import fjb.mock.model.dto.schoolDTO.SchoolImageResponseDTO;
import fjb.mock.model.dto.schoolDTO.SchoolUtilitiesResponseDTO;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.math.BigInteger;
import java.util.List;

@Data
public class AdminSchoolResponseDTO {
    private Long id;
    private Long OwnerId;
    private String name;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")
    private String email;

    private ProvinceDTO city;
    private DistrictDTO district;
    private WardDTO ward;

    private String street;

    private String website;
    private BigInteger minFee;
    private BigInteger maxFee;
    @Length(max = 4000)
    private String introduction;

    private SchoolTypeEnum type;
    private ChildReceivingAgeEnum childReceivingAge;
    private EducationMethodEnum educationMethod;

    private SchoolStatusEnum status;

    List<SchoolImageResponseDTO> images;
    SchoolImageResponseDTO mainImage;

    private List<SchoolFacilitiesResponseDTO> schoolFacilities;
    private List<SchoolUtilitiesResponseDTO> schoolUtilities;
}
