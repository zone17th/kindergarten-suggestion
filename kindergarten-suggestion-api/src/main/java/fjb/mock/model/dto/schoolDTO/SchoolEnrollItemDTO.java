package fjb.mock.model.dto.schoolDTO;

import fjb.mock.model.dto.addressDTO.DistrictDTO;
import fjb.mock.model.dto.addressDTO.ProvinceDTO;
import fjb.mock.model.dto.addressDTO.WardDTO;
import fjb.mock.model.dto.userDTO.FeedbackEnrolledResponseDTO;
import lombok.Data;
import java.util.List;

@Data
public class SchoolEnrollItemDTO {
    private Long id;
    private String name;
    private String phoneNumber;
    private String email;
    private ProvinceDTO city;
    private DistrictDTO district;
    private WardDTO ward;
    private String street;
    private String website;
    private Integer totalFeedbacks;
    List<FeedbackEnrolledResponseDTO> feedback;
}
