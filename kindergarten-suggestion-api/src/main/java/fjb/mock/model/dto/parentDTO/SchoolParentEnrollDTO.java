package fjb.mock.model.dto.parentDTO;

import lombok.Data;

@Data
public class SchoolParentEnrollDTO {
    private Long id;
}
