package fjb.mock.model.dto.schoolDTO;

import lombok.Data;

@Data
public class SchoolUtilitiesResponseDTO {
    private UtilityResponseDTO utility;
}
