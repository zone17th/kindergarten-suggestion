package fjb.mock.model.dto.userDTO;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class ResetPasswordRequestDTO {
    @NotBlank
    private String token;
    @NotBlank
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d).{7,}$",
            message = "Use at least one number, one numeral, and seven characters.")
    private String password;
}
