package fjb.mock.model.dto.userDTO;

import fjb.mock.model.appEnum.UserRoleEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AdminLoginResponseDTO {
    private String accessToken;
    private UserRoleEnum role;
}
