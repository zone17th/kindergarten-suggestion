package fjb.mock.model.dto.schoolDTO;

import fjb.mock.model.appEnum.ChildReceivingAgeEnum;
import fjb.mock.model.appEnum.EducationMethodEnum;
import fjb.mock.model.appEnum.SchoolTypeEnum;
import fjb.mock.model.dto.addressDTO.DistrictDTO;
import fjb.mock.model.dto.addressDTO.ProvinceDTO;
import fjb.mock.model.dto.addressDTO.WardDTO;
import fjb.mock.model.dto.userDTO.FeedBackWithUserResponseDTO;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class SchoolDetailResponseDTO {
    private Long id;
    private String name;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")
    private String email;
    private ProvinceDTO city;
    private DistrictDTO district;
    private WardDTO ward;
    private String street;
    private String website;
    @Enumerated(EnumType.STRING)
    private SchoolTypeEnum type;
    @Enumerated(EnumType.STRING)
    private ChildReceivingAgeEnum childReceivingAge;
    @Enumerated(EnumType.STRING)
    private EducationMethodEnum educationMethod;
    private BigInteger minFee;
    private BigInteger maxFee;
    @Column(length = 4000)
    private String introduction;
    private boolean rate;
    private Double avgRating;
    private String mainImage;
    private LocalDateTime createdDate;
    List<SchoolImageResponseDTO> images;
    List<FeedBackWithUserResponseDTO> feedBacks;
    List<SchoolFacilitiesResponseDTO> schoolFacilities;
    List<SchoolUtilitiesResponseDTO> schoolUtilities;
}
