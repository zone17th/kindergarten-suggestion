package fjb.mock.model.dto.schoolDTO;

import fjb.mock.model.appEnum.UtilityEnum;
import lombok.Data;

@Data
public class UtilityResponseDTO {
    private Long id;
    private UtilityEnum code;
    private String name;
}
