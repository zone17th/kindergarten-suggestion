package fjb.mock.services.serviceImpl;

import fjb.mock.model.dto.schoolDTO.adminSchoolDTO.AdminSchoolRequestDTO;
import fjb.mock.model.entities.School;
import fjb.mock.model.local.District;
import fjb.mock.model.local.Province;
import fjb.mock.model.local.Ward;
import fjb.mock.repository.DistrictRepository;
import fjb.mock.repository.ProvinceRepository;
import fjb.mock.repository.WardRepository;
import fjb.mock.services.AddressService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {
    private final ProvinceRepository provinceRepository;
    private final DistrictRepository districtRepository;
    private final WardRepository wardRepository;

    public AddressServiceImpl(ProvinceRepository provinceRepository, DistrictRepository districtRepository, WardRepository wardRepository) {
        this.provinceRepository = provinceRepository;
        this.districtRepository = districtRepository;
        this.wardRepository = wardRepository;
    }

    @Override
    public List<Province> getAllProvince() {
        return provinceRepository.getAllByOrderByLevelAsc();
    }

    @Override
    public Optional<Province> getProvinceById(Long id) {
        return provinceRepository.findById(id);
    }

    @Override
    public Optional<District> getDistrictById(Long id) {
        return districtRepository.findById(id);
    }

    @Override
    public Optional<Ward> getWardById(Long id) {
        return wardRepository.findById(id);
    }

    @Override
    public List<District> getAllDistrictByProvinceId(Long id) {
        return districtRepository.getAllByProvinceId(id);
    }

    @Override
    public List<Ward> getAllWardByDistrictId(Long id) {
        return wardRepository.getAllByDistrictId(id);
    }

    public ResponseEntity<?> setAddress(School school, AdminSchoolRequestDTO schoolRequest) {
        Optional<Province> city = getProvinceById(schoolRequest.getCityId());
        Optional<District> district = getDistrictById(schoolRequest.getDistrictId());
        Optional<Ward> ward = getWardById(schoolRequest.getWardId());

        if (city.isEmpty() || district.isEmpty() || ward.isEmpty()) {
            return ResponseEntity.badRequest().body("Address is not found!");
        }
        school.setCity(city.get());
        school.setDistrict(district.get());
        school.setWard(ward.get());

        return null;
    }
}
