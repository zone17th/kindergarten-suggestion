package fjb.mock.services.serviceImpl;

import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.entities.*;
import fjb.mock.repository.FeedBackRepository;
import fjb.mock.repository.SchoolRepository;
import fjb.mock.services.SchoolService;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
public class SchoolServiceImpl implements SchoolService {
    private final SchoolRepository schoolRepository;
    private final FeedBackRepository feedBackRepository;

    public SchoolServiceImpl(SchoolRepository schoolRepository, FeedBackRepository feedBackRepository) {
        this.schoolRepository = schoolRepository;
        this.feedBackRepository = feedBackRepository;
    }

    @Override
    public School save(School school) {
        return schoolRepository.save(school);
    }

    @Override
    public void saveAll(List<School> schools) {
        schoolRepository.saveAll(schools);
    }

    @Override
    public Optional<School> findBySchoolIdAndStatusAndDeletedFalse(SchoolStatusEnum status, Long id) {
        return schoolRepository.findByIdAndStatusAndDeletedFalse(id, status);
    }

    @Override
    public Page<School> getPageData(Specification<School> spec, Pageable pageable) {
        Specification<School> undeletedAndStatusPublished =
                (root, query, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.equal(root.get("deleted"), false),
                          criteriaBuilder.equal(root.get("status"), SchoolStatusEnum.PUBLISHED));

        spec = spec == null ? undeletedAndStatusPublished: spec.and(undeletedAndStatusPublished);

        return schoolRepository.findAll(spec, pageable);

    }

    @Override
    public Page<School> getPageData(Pageable pageable) {
        return null;
    }

    @Override
    public List<FeedBack> getTopNFeedBack() {
        return feedBackRepository.getTopByDeletedFalse();
    }

    @Override
    public BigInteger findMinFee() {
        return schoolRepository.findMinFee();
    }

    @Override
    public BigInteger findMaxFee() {
        return schoolRepository.findMaxFee();
    }

    @Override
    public Page<School> findAll(Specification<School> spec, Pageable pageable) {
        return schoolRepository.findAll(spec, pageable);
    }


    @Override
    public List<School> findAllByAdmin(Specification<School> spec, Sort sort) {
        Specification<School> newSpec = null;
        newSpec = (((root, query, criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.or(
                    criteriaBuilder.equal(root.get("deleted"), false),
                    criteriaBuilder.notEqual(root.get("status"), SchoolStatusEnum.SAVED));
            return predicate;
        }));
        spec = spec == null ? newSpec : spec.and(newSpec);
        return schoolRepository.findAll(spec,sort);
    }

    @Override
    public List<School> findAllByStatusAndRole(Specification<School> spec, Sort sort) {

        return null;
    }


    @Override
    public Optional<School> findBySchoolIdAndDeletedFalse(Long id) {
        return schoolRepository.findByIdAndDeletedFalse(id);
    }

    @Override
    public Optional<School> findByEmailAndDeletedFalse(String email) {
        return schoolRepository.findByEmailAndDeletedFalse(email);
    }

    @Override
    public Optional<School> findByPhoneNumberAndDeletedFalse(String phone) {
        return schoolRepository.findByPhoneNumberAndDeletedFalse(phone);
    }

    @Override
    public List<School> findAllByStatusAndDeletedFalse(SchoolStatusEnum status) {
        return schoolRepository.findAllByStatusAndDeletedFalse(status);
    }

}
