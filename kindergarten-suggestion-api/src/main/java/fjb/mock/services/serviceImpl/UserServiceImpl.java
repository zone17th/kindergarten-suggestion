package fjb.mock.services.serviceImpl;

import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.entities.Account_;
import fjb.mock.model.entities.User;
import fjb.mock.model.entities.User_;
import fjb.mock.repository.UserRepository;
import fjb.mock.services.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findByID(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findUserByEmail(String email) {
        return userRepository.findUserByAccountEmailAndDeletedFalse(email);
    }

    @Override
    public void createNew(User user) {
        user.setDeleted(false);
        userRepository.save(user);
    }

    @Override
    public void update(User user) {
        user.setDeleted(false);
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByPhoneNumber(String phoneNumber) {
        return userRepository.findByPhoneNumber(phoneNumber);
    }


    @Override
    public boolean existUserByPhoneNumber(String phoneNumber) {
        return userRepository.existsByPhoneNumberAndDeletedFalse(phoneNumber);
    }

    @Override
    public Page<User> findParentsByAdmin(String keyword, Pageable pageable) {
        return userRepository.findParentsByAdmin( keyword, pageable);
    }

    @Override
    public Page<User> findParentsByOwnerSchool(Long userId, String keyword, Pageable pageable) {
        return userRepository.findParentsByOwnerSchool(userId, keyword, pageable);
    }

    @Override
    public Optional<User> findUserAndAccountByAccountEmailAndDeletedFalse(String email) {
        return userRepository.findUserAndAccountByAccountEmailAndDeletedFalse(email);
    }

    @Override
    public Optional<User> getUserById(Long id) {
        return userRepository.findByIdAndDeletedFalse(id);
    }


    @Override
    public Page<User> findAll(Specification<User> spec, Pageable pageable) {
        return userRepository.findAll(spec, pageable);
    }

    @Override
    public void delete(User user) {
        user.setDeleted(true);
        userRepository.save(user);
    }

    @Override
    public List<User> findAllSchoolOwner() {
        return userRepository.findAllByAccount_RoleAndDeletedFalse(UserRoleEnum.SCHOOL_OWNER);
    }

    @Override
    public Optional<User> findAdmin() {
        return userRepository.findUserByAccount_RoleAndDeletedFalse(UserRoleEnum.ADMIN);
    }
}