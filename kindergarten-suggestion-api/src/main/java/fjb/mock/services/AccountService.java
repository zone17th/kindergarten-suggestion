package fjb.mock.services;

import java.util.Optional;

import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.dto.userDTO.LoginResponseDTO;
import fjb.mock.model.entities.Account;
import fjb.mock.model.entities.User;
import org.springframework.stereotype.Service;

public interface AccountService {
    Optional<Account> findAllByRole(UserRoleEnum role);
    Optional<Account> findByID(Long id);
    boolean checkExistEmail(String email);
    boolean checkExistPhone(String phone);
    void createNew(Account account, User user);
    void createNew(Account account);
    Optional<Account> findByEmailAndPasswordAndActiveTrueAndDeletedFalse(String email, String password);
    Optional<Account> findByEmailAndActiveTrueAndDeletedFalse(String email);
    Optional<Account> findByEmailAndActiveFalseAndDeletedFalse(String email);
    void update(Account account);
    void activeAccount(Account account);
    void delete(Account account);

    void createNewByAdmin(Account account);
    LoginResponseDTO login(String email,String password);
    public String generateRandomPassword();
}