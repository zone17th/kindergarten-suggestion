package fjb.mock.services;


import fjb.mock.model.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> findByID(Long id);
    Optional<User> findUserByEmail(String email);
    Optional<User> getUserById(Long id);
    Optional<User> findUserAndAccountByAccountEmailAndDeletedFalse(String email);

    void createNew(User user);

    void update(User user);

    Optional<User> findByPhoneNumber(String phoneNumber);

    Page<User> findAll(Specification<User> spec, Pageable pageable);

    void delete(User user);
    List<User> findAllSchoolOwner();

    Optional<User> findAdmin();

    boolean existUserByPhoneNumber(String phoneNumber);

    Page<User> findParentsByAdmin(String keyword, Pageable pageable);

    Page<User> findParentsByOwnerSchool(Long userId, String keyword, Pageable pageable);

}