package fjb.mock.services;

import java.util.List;
import java.util.Optional;

import fjb.mock.model.entities.FeedBack;
import fjb.mock.model.entities.School;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface FeedBackService {
    void createNew(FeedBack feedBack);

    List<FeedBack> getTopFeedback();

    Optional<FeedBack> findBySchoolIdAndUserId(Long id, Long userId);

    List<FeedBack> findBySchoolIdAndDeletedFalse(Long id);

    Page<FeedBack> findAllBySchoolIdAndDeletedFalse(Long id, Pageable pageable);

    Page<FeedBack> findAll(Long id, Specification<FeedBack> spec, Pageable pageable);

    List<FeedBack> findFeedbackBySchoolIdAndUserId(Long id, Long userId);

}
