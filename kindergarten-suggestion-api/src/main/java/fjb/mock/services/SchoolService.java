package fjb.mock.services;

import java.math.BigInteger;
import java.util.Optional;

import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.entities.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface SchoolService {
    School save(School school);
    void saveAll(List<School> schools);
    Optional<School> findBySchoolIdAndStatusAndDeletedFalse(SchoolStatusEnum status, Long id);
    Optional<School> findBySchoolIdAndDeletedFalse(Long id);
    Optional<School> findByEmailAndDeletedFalse(String email);
    Optional<School> findByPhoneNumberAndDeletedFalse(String phone);
    List<School> findAllByStatusAndDeletedFalse(SchoolStatusEnum status);
    Page<School> getPageData(Specification<School> spec, Pageable pageable);
    Page<School> getPageData( Pageable pageable);
    List<FeedBack> getTopNFeedBack();
    Page<School> findAll(Specification<School> spec, Pageable pageable);

    List<School> findAllByAdmin(Specification<School> spec, Sort sort);
    List<School> findAllByStatusAndRole(Specification<School> spec, Sort sort);

    BigInteger findMinFee();
    BigInteger findMaxFee();
}
