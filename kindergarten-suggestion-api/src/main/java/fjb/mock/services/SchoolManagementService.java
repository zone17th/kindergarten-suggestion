package fjb.mock.services;

import fjb.mock.model.entities.SchoolManagement;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface SchoolManagementService {
    Optional<SchoolManagement> findSchoolManagementBySchoolIdAndUserIdAndDeletedFalse(Long schoolId, Long userId);
    List<SchoolManagement> findSchoolManagementByUserIdAndDeletedFalse(Long id);
}
