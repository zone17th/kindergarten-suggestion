package fjb.mock.services;

import fjb.mock.model.dto.schoolDTO.adminSchoolDTO.AdminSchoolRequestDTO;
import fjb.mock.model.entities.School;
import fjb.mock.model.local.District;
import fjb.mock.model.local.Province;
import fjb.mock.model.local.Ward;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface AddressService {
    List<Province> getAllProvince();

    Optional<Province> getProvinceById(Long id);

    Optional<District> getDistrictById(Long id);

    Optional<Ward> getWardById(Long id);


    List<District> getAllDistrictByProvinceId(Long id);

    List<Ward> getAllWardByDistrictId(Long id);
    ResponseEntity<?> setAddress(School school, AdminSchoolRequestDTO schoolRequest);
}
