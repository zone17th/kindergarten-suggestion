package fjb.mock.repository;

import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.entities.School;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface SchoolRepository extends BaseRepository<School, Long> {
    Optional<School> findByIdAndStatusAndDeletedFalse(Long id, SchoolStatusEnum status);
    List<School> findAllByStatusAndDeletedFalse(SchoolStatusEnum status);
    Optional<School> findByIdAndDeletedFalse(Long id);
    Optional<School> findByEmailAndDeletedFalse(String email);
    Optional<School> findByPhoneNumberAndDeletedFalse(String phone);

    @Query("SELECT MIN(s.minFee) FROM School s WHERE s.deleted = false AND s.status = 'PUBLISHED'")
    BigInteger findMinFee();

    @Query("SELECT MAX(s.minFee) FROM School s WHERE s.deleted = false AND s.status = 'PUBLISHED'")
    BigInteger findMaxFee();

}