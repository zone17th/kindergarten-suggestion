package fjb.mock.repository;

import fjb.mock.model.entities.enumEntity.Facility;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FacilitiesRepository extends BaseRepository<Facility,Long> {
    List<Facility> findAllByDeletedFalse();
    Optional<Facility> findByIdAndDeletedFalse(Long id);
}
