package fjb.mock.repository;

import fjb.mock.model.entities.FeedBack;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FeedBackRepository extends BaseRepository<FeedBack, Long> {
    @Query("SELECT f FROM FeedBack f  WHERE f.deleted = false ORDER BY f.avgRating DESC LIMIT 4")
    List<FeedBack> getTopByDeletedFalse();

    @Query("SELECT f FROM FeedBack f WHERE f.deleted = false AND f.school.id=:id")
    List<FeedBack> findBySchoolIdAndDeletedFalse(Long id);

//    Page<FeedBack> getPageData(Long id, Specification<FeedBack> spec, Pageable pageable);
//    Page<FeedBack> findAll(Specification<FeedBack> spec, Pageable pageable);
    Page<FeedBack> findAllBySchoolIdAndDeletedFalse(Long id, Pageable pageable);

    Optional<FeedBack> findBySchoolIdAndUserIdAndDeletedFalse(Long id, Long userId);

    List<FeedBack> findBySchoolIdAndUserId(Long id, Long userId);

}