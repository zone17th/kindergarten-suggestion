package fjb.mock.repository;

import fjb.mock.model.entities.SchoolEnrollDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface SchoolEnrollDetailRepository extends BaseRepository<SchoolEnrollDetail, Long> {
    Page<SchoolEnrollDetail> findAllByUserIdAndStatusAndDeletedFalse(Long id, Boolean status, Pageable pageable);
    Optional<SchoolEnrollDetail> findBySchoolIdAndUserIdAndDeletedFalse(Long schoolId, Long userId);
    List<SchoolEnrollDetail> findAllBySchoolIdAndUserIdAndDeletedFalse(Long schoolId, Long userId);
    List<SchoolEnrollDetail> findAllByUserIdAndStatusTrueAndDeletedFalse(Long id);


}
