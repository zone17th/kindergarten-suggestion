package fjb.mock.resource.adminResource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fjb.mock.mapper.UserMapper;
import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.dto.userDTO.CreateNewUserDTO;
import fjb.mock.model.dto.userDTO.UserProfileDTO;
import fjb.mock.model.dto.userDTO.UserResponseDTO;
import fjb.mock.model.entities.Account;
import fjb.mock.model.entities.Account_;
import fjb.mock.model.entities.User;
import fjb.mock.model.entities.User_;
import fjb.mock.security.SecurityUtil;
import fjb.mock.services.AccountService;
import fjb.mock.services.UserService;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/admin/user")
public class AdminUserResource {

    private final UserService userService;
    private final AccountService accountService;
    private final UserMapper userMapper;

    public AdminUserResource(UserService userService, AccountService accountService, UserMapper userMapper) {
        this.userService = userService;
        this.accountService = accountService;
        this.userMapper = userMapper;
    }

    @GetMapping("/list")
    public ResponseEntity<?> getAllUser(@RequestParam(defaultValue = "0") Integer page,
                                        @RequestParam(defaultValue = "10") Integer size,
                                        @RequestParam(name = "q", required = false) Optional<String> keywordOpt, Sort sort) {
        Specification<User> spec = null;
        if (keywordOpt.isPresent()) {
            String keyword = keywordOpt.get();
            spec = ((root, query, criteriaBuilder) -> {
                Predicate predicate = criteriaBuilder.or(
                        criteriaBuilder.like(root.get(User_.FULL_NAME), "%" + keyword + "%"),
                        criteriaBuilder.like(root.get(User_.PHONE_NUMBER), "%" + keyword + "%"),
                        criteriaBuilder.like(root.get(User_.account).get(Account_.EMAIL), "%" + keyword + "%"));

                List<Join<User, ?>> joinsToCheck = Arrays.asList(
                        root.join(User_.city, JoinType.LEFT),
                        root.join(User_.district, JoinType.LEFT),
                        root.join(User_.ward, JoinType.LEFT)
                );
                // Kiểm tra và thêm điều kiện cho các trường không null và "LIKE"
                for (Join<User, ?> join : joinsToCheck) {
                    Path<String> path = join.get("name");
                    Predicate notNull = criteriaBuilder.isNotNull(path);
                    Predicate like = criteriaBuilder.like(path, "%" + keyword + "%");
                    predicate = criteriaBuilder.or(predicate, criteriaBuilder.and(notNull, like));
                }



                // Kiểm tra và thêm điều kiện cho trường "role" nếu keyword là giá trị enum hợp lệ
                boolean isUserRoleEnum = Arrays.stream(UserRoleEnum.values())
                        .anyMatch(roleEnum -> roleEnum.name().equalsIgnoreCase(keyword));

                if (isUserRoleEnum) {
                    Path<UserRoleEnum> roleEnumPath = root.get(User_.account).get(Account_.ROLE);
                    predicate = criteriaBuilder.or(
                            predicate,
                            criteriaBuilder.equal(roleEnumPath, UserRoleEnum.valueOf(keyword.toUpperCase()))
                    );
                }
                return predicate;
            });
        }
        Specification<User> undeleted =
                (root, query, criteriaBuilder) -> criteriaBuilder.and(
                            criteriaBuilder.equal(root.get(User_.DELETED), false),
                            criteriaBuilder.equal(root.get(User_.account).get(Account_.DELETED), false));

        spec = spec == null ? undeleted : spec.and(undeleted);
        sort = Sort.by(User_.LAST_MODIFIED_DATE).descending();
        PageRequest pageRequest = PageRequest.of(page, size, sort);
        Page<User> userList = userService.findAll(spec, pageRequest);
        Page<UserResponseDTO> userResponseDTOList = userList.map(userMapper::toUserDTO);
        return ResponseEntity.ok().body(userResponseDTOList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserInfo(@PathVariable(name = "id") Long id) {
        if (!SecurityUtil.isAdmin()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        Optional<User> userOtp = userService.getUserById(id);
        if (userOtp.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        UserProfileDTO userProfileDTO = userMapper.toProfileDTO(userOtp.get());
        return ResponseEntity.ok(userProfileDTO);
    }

    @DeleteMapping("/deleted/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(name = "id") Long id) {
        if (!SecurityUtil.isAdmin()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        Optional<User> userOptional = userService.findByID(id);
        if (userOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        userService.delete(userOptional.get());
        accountService.delete(userOptional.get().getAccount());
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody CreateNewUserDTO userDTO) {
        if (!SecurityUtil.isAdmin()){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        if (accountService.checkExistEmail(userDTO.getEmail())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Email is existed!");
        }
        if (userService.existUserByPhoneNumber(userDTO.getPhoneNumber())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Phone number is existed!");
        }
        User user = new User();
        BeanUtils.copyProperties(userDTO, user);
        userService.createNew(user);
        Account account = new Account();
        BeanUtils.copyProperties(userDTO, account);
        account.setActive(userDTO.getActive() == null || userDTO.getActive().equals("true"));
        account.setPassword(accountService.generateRandomPassword());
        user.setAccount(account);
        accountService.createNewByAdmin(account);
        return ResponseEntity.ok(account);
    }

    @PutMapping("/updateStatus/{id}")
    public ResponseEntity<?> updateUserStatus(@PathVariable(name = "id") Long id) {
        if (!SecurityUtil.isAdmin()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        Optional<User> userOptional = userService.getUserById(id);
        if (userOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        User user = userOptional.get();
        Account account = user.getAccount();
        boolean status = account.getActive();
        user.getAccount().setActive(!status); // Cập nhật trạng thái "active"

        // Lưu người dùng đã cập nhật vào cơ sở dữ liệu
        userService.update(user);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateProfile(@PathVariable(name = "id") Long id, @Valid @RequestBody CreateNewUserDTO userDTO) {
        if (!SecurityUtil.isAdmin()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        Optional<User> userOtp = userService.getUserById(id);
        if (userOtp.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        User user = userOtp.get();
        Account account = user.getAccount();
        if (accountService.checkExistEmail(userDTO.getEmail()) && !account.getEmail().equals(userDTO.getEmail())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Email is existed!");
        }
        if (userService.existUserByPhoneNumber(userDTO.getPhoneNumber()) && !user.getPhoneNumber().equals(userDTO.getPhoneNumber())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Phone number is existed!");
        }

        BeanUtils.copyProperties(userDTO, user);
        userService.update(user);
        BeanUtils.copyProperties(userDTO, account);
        account.setActive(userDTO.getActive().equals("true"));
        accountService.update(account);
        return ResponseEntity.ok("Successfully!");
    }

    @GetMapping("role")
    public ResponseEntity<?> getRole() {
        List<String> roleList = new ArrayList<>();
        for(UserRoleEnum role:UserRoleEnum.values()) {
            roleList.add(role.name());
        }
        return ResponseEntity.ok(roleList);
    }

    @GetMapping("/owner")
    public ResponseEntity<?> getAllOwner() {

        List<User> accountList = userService.findAllSchoolOwner();
        if (accountList.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        List<UserProfileDTO>userProfileDTOList = accountList.stream().map(userMapper::toProfileDTO).collect(Collectors.toList());
        return ResponseEntity.ok(userProfileDTOList);
    }



}
