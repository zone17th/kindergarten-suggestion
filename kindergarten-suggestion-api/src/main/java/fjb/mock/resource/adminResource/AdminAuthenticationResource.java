package fjb.mock.resource.adminResource;

import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.dto.userDTO.LoginRequestDTO;
import fjb.mock.model.dto.userDTO.LoginResponseDTO;
import fjb.mock.model.entities.Account;
import fjb.mock.services.AccountService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/admin/login")
public class AdminAuthenticationResource {
    private final AccountService accountService;

    public AdminAuthenticationResource(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping()
    public ResponseEntity<?> adminLogin(@RequestBody @Valid LoginRequestDTO loginRequestDTO) {
        // Kiểm tra trong database có tồn tại account với active true và deleted false
        Optional<Account> accountOptional = accountService
                .findByEmailAndActiveTrueAndDeletedFalse(loginRequestDTO.getEmail());
        // Nếu không tồn tại thông báo lỗi
        if (accountOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        // Kiểm tra role hiện tại của account
        if (!accountOptional.get().getRole().equals(UserRoleEnum.ADMIN) &&
                !accountOptional.get().getRole().equals(UserRoleEnum.SCHOOL_OWNER)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        LoginResponseDTO loginResponseDTO = accountService.login(loginRequestDTO.getEmail(),
                loginRequestDTO.getPassword());
        if (loginResponseDTO == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok(loginResponseDTO);
    }
}
