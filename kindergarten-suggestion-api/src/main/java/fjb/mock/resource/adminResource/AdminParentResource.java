package fjb.mock.resource.adminResource;

import fjb.mock.mapper.*;
import fjb.mock.model.appEnum.RequestStatusEnum;
import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.dto.parentDTO.ParentDetailResponseDTO;
import fjb.mock.model.dto.parentDTO.ParentSchoolResponseDTO;
import fjb.mock.model.dto.schoolDTO.SchoolEnrollDetailDTO;
import fjb.mock.model.dto.userDTO.UserAccountSchoolResponseDTO;
import fjb.mock.model.entities.*;
import fjb.mock.security.SecurityUtil;
import fjb.mock.services.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api/admin/parent")
public class AdminParentResource {
    private final UserService userService;
    private final UserMapper userMapper;
    private final ParentMapper parentMapper;
    private final FeedBackService feedBackService;
    private final SchoolEnrollDetailMapper schoolEnrollDetailMapper;
    private final FeedBackMapper feedBackMapper;
    private final SchoolService schoolService;
    private final SchoolMapper schoolMapper;
    private final SchoolEnrollDetailService schoolEnrollDetailService;
    private final RequestCounsellingService requestCounsellingService;
    private final SchoolManagementService schoolManagementService;

    public AdminParentResource(UserService userService, UserMapper userMapper, ParentMapper parentMapper, FeedBackService feedBackService, SchoolEnrollDetailMapper schoolEnrollDetailMapper, FeedBackMapper feedBackMapper, SchoolService schoolService, SchoolMapper schoolMapper, SchoolEnrollDetailService schoolEnrollDetailService, RequestCounsellingService requestCounsellingService, SchoolManagementService schoolManagementService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.parentMapper = parentMapper;
        this.feedBackService = feedBackService;
        this.schoolEnrollDetailMapper = schoolEnrollDetailMapper;
        this.feedBackMapper = feedBackMapper;
        this.schoolService = schoolService;
        this.schoolMapper = schoolMapper;
        this.schoolEnrollDetailService = schoolEnrollDetailService;
        this.requestCounsellingService = requestCounsellingService;
        this.schoolManagementService = schoolManagementService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> getParents(@RequestParam(defaultValue = "0", required = false) Integer page,
                                        @RequestParam(defaultValue = "10", required = false) Integer size,
                                        @RequestParam(name = "q") Optional<String> keywordOpt) {

        String email = SecurityUtil.getCurrentAccount();
        Optional<User> userOptional = userService.findUserByEmail(email);
        if (SecurityUtil.isParents() || userOptional.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);  // 403 Forbidden
        }
        Sort sort = Sort.by(Sort.Order.desc(User_.LAST_MODIFIED_DATE));
        PageRequest pageRequest = PageRequest.of(page, size, sort);
        String keyword = keywordOpt.orElse("");
        int enroll;
        if (SecurityUtil.isSchoolOwner()) {
            Page<User> users = userService.findParentsByOwnerSchool(userOptional.get().getId(), keyword, pageRequest);
            Page<UserAccountSchoolResponseDTO> userAccountSchoolResponseDTOS =
                    users.map(userMapper::toUserSchool);
            for (UserAccountSchoolResponseDTO u : userAccountSchoolResponseDTOS) {
                enroll = 0;
                for (SchoolEnrollDetailDTO s : u.getSchoolEnrollDetails()) {
                    if (!s.getDeleted() && s.getStatus()) {
                        enroll++;
                    }
                }
                u.setEnroll(enroll);
            }
            return ResponseEntity.ok().body(userAccountSchoolResponseDTOS);
        }
        Page<User> users = userService.findParentsByAdmin(keyword, pageRequest);
        Page<UserAccountSchoolResponseDTO> userAccountSchoolResponseDTOS =
                users.map(userMapper::toUserSchool);
        for (UserAccountSchoolResponseDTO u : userAccountSchoolResponseDTOS) {
            enroll = 0;
            for (SchoolEnrollDetailDTO s : u.getSchoolEnrollDetails()) {
                if (!s.getDeleted() && s.getStatus()) {
                    enroll++;
                }
            }
            u.setEnroll(enroll);
        }
        return ResponseEntity.ok(userAccountSchoolResponseDTOS);
    }

//    @GetMapping("/{id}")
//    public ResponseEntity<?> getParentDetail(@PathVariable(name = "id") Long id) {
//        Optional<User> userOpt = userService.getUserById(id);
//        String email = SecurityUtil.getCurrentAccount();
//        Optional<User> currentUserOpt = userService.findUserByEmail(email);
//        if (userOpt.isEmpty() || currentUserOpt.isEmpty()) {
//            return ResponseEntity.notFound().build();
//        }
//        User user = userOpt.get();//Parent detail
//        Account account = user.getAccount();
//        User currentUser = currentUserOpt.get();//User login
//        if (SecurityUtil.isParents() || account.getRole() != UserRoleEnum.PARENTS) {
//            return new ResponseEntity<>(HttpStatus.FORBIDDEN);  // 403 Forbidden≈
//        }
//        ParentDetailResponseDTO parentDetailResponseDTO = parentMapper.toDTO(user);//Get được thông tin
//        List<School> schoolAllPublishedList;
//        if (SecurityUtil.isAdmin()) {
//            schoolAllPublishedList = schoolService.findAllByStatusAndDeletedFalse(SchoolStatusEnum.PUBLISHED);
//            if (schoolAllPublishedList.isEmpty()) {
//                return ResponseEntity.noContent().build();
//            }
//        } else {
//            List<SchoolManagement> schoolManagements = currentUser.getSchoolManagements();
//            if (schoolManagements.isEmpty()) {
//                return ResponseEntity.noContent().build();
//            }
//            schoolAllPublishedList = schoolManagements.stream()
//                    .map(SchoolManagement::getSchool)
//                    .filter(e -> e.getStatus().equals(SchoolStatusEnum.PUBLISHED))
//                    .collect(Collectors.toList());
//
//        }
//        List<SchoolDetailResponseDTO> detailList = schoolMapper.toDTO(schoolAllPublishedList);
//        parentDetailResponseDTO.setSchool(detailList);
//        return ResponseEntity.ok(parentDetailResponseDTO);
//    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getParentDetail(@PathVariable(name = "id") Long userId) {
        Optional<User> userOpt = userService.getUserById(userId);
        String email = SecurityUtil.getCurrentAccount();
        Optional<User> userOptional = userService.findUserByEmail(email);
        if (userOpt.isEmpty() || userOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        if (SecurityUtil.isParents()
                || userOpt.get().getAccount().getRole() == UserRoleEnum.ADMIN
                || userOpt.get().getAccount().getRole() == UserRoleEnum.SCHOOL_OWNER) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);  // 403 Forbidden≈
        }
        User user = userOpt.get();//Parent detail
        ParentDetailResponseDTO parentDetailResponseDTO = parentMapper.toDTO(user);
        List<RequestStatusEnum> statuses = new ArrayList<>();
        statuses.add(RequestStatusEnum.OPEN);
        statuses.add(RequestStatusEnum.CLOSED);
        List<RequestCounselling> requestCounsellings =
                requestCounsellingService.findAllByUserIdAndStatusInAndDeletedFalse(userId, statuses);
        List<SchoolEnrollDetail> schoolEnrollDetailList = schoolEnrollDetailService
                .findAllByUserIdAndStatusTrueAndDeletedFalse(userId);
        List<Long> schoolEnrollIdList = schoolEnrollDetailList.stream()
                .map(detail -> detail.getSchool().getId())
                .toList();

        List<School> schoolAllPublishedList = schoolService.findAllByStatusAndDeletedFalse(SchoolStatusEnum.PUBLISHED);
        List<Long> schoolAllPublishedIdList = schoolAllPublishedList.stream()
                .map(BaseEntity::getId)
                .toList();

        if (!SecurityUtil.isAdmin()) {
            List<SchoolManagement> schoolManagements = schoolManagementService
                    .findSchoolManagementByUserIdAndDeletedFalse(userOptional.get().getId());

            // Check parent có là phụ huynh của chủ trường này ko
            boolean hasCommonSchoolId = schoolEnrollDetailList.stream()
                    .anyMatch(enrollDetail ->
                            schoolManagements.stream()
                                    .anyMatch(schoolManagement ->
                                            enrollDetail.getSchool().getId().equals(schoolManagement.getSchool().getId())
                                    )
                    );
            boolean checkHasRequest = requestCounsellings
                    .stream().anyMatch(request ->
                            schoolManagements.stream()
                                    .anyMatch(schoolManagement ->
                                            request.getSchool().getId().equals(schoolManagement.getSchool().getId())
                                    ));
            if (!hasCommonSchoolId && !checkHasRequest) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }

            List<Long> schoolManagementIds = schoolManagements.stream()
                    .map(schoolManagement -> schoolManagement.getSchool().getId())
                    .toList();
            List<Long> ids = schoolManagementIds.stream()
                    .filter(id -> !schoolEnrollIdList.contains(id))
                    .toList();
            if (ids.isEmpty()) {
                return ResponseEntity.ok(parentDetailResponseDTO);
            }
            List<School> filterSchoolList = schoolAllPublishedList.stream()
                    .filter(detail -> ids.contains(detail.getId()))
                    .toList();
            parentDetailResponseDTO.setSchool(schoolMapper.toDTO(filterSchoolList));
            return ResponseEntity.ok(parentDetailResponseDTO);
        }
        List<Long> ids = schoolAllPublishedIdList.stream()
                .filter(id -> !schoolEnrollIdList.contains(id))
                .toList();
        List<School> filterSchoolList = schoolAllPublishedList.stream()
                .filter(detail -> ids.contains(detail.getId()))
                .toList();
        parentDetailResponseDTO.setSchool(schoolMapper.toDTO(filterSchoolList));
        return ResponseEntity.ok(parentDetailResponseDTO);
    }

    @PutMapping("/unEnroll/{id}/{schoolId}")
    public ResponseEntity<?> parentUnEnroll(@PathVariable(name = "id") Long id,
                                            @PathVariable(name = "schoolId") Long schoolId) {
        Optional<User> userOpt = userService.getUserById(id);
        String email = SecurityUtil.getCurrentAccount();
        Optional<User> currentUserOpt = userService.findUserByEmail(email);
        if (userOpt.isEmpty() || currentUserOpt.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        User user = userOpt.get();
        Optional<SchoolEnrollDetail> schoolEnrollDetailOpt = user.getSchoolEnrollDetails()
                .stream()
                .filter(detail -> detail.getSchool().getId().equals(schoolId))
                .findFirst();
        if (schoolEnrollDetailOpt.isPresent()) {
            boolean isAdmin = SecurityUtil.isAdmin();
            List<Long> schoolManageIdList = currentUserOpt.get().getSchoolManagements()
                    .stream()
                    .map(detail -> detail.getSchool().getId())
                    .toList();
            if (isAdmin || schoolManageIdList.contains(schoolId)) {
                SchoolEnrollDetail schoolEnrollDetail = schoolEnrollDetailOpt.get();
                boolean status = schoolEnrollDetail.getStatus();
                schoolEnrollDetail.setStatus(!status);
                List<SchoolEnrollDetail> userSchoolEnrollDetails = user.getSchoolEnrollDetails();
                int indexToUpdate = userSchoolEnrollDetails.indexOf(schoolEnrollDetail);
                userSchoolEnrollDetails.set(indexToUpdate, schoolEnrollDetail);
                userService.update(user);
                return new ResponseEntity<>(HttpStatus.CREATED);
            }
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/enroll/{id}/{schoolId}")
    public ResponseEntity<?> ParentEnroll(@PathVariable(name = "id") Long id,
                                          @PathVariable(name = "schoolId") Long schoolId) {
        if (SecurityUtil.isAdmin()) {
            return schoolEnrollDetailService.createEnroll(id, schoolId);
        }
        String email = SecurityUtil.getCurrentAccount();
        Optional<User> userOpt = userService.findUserByEmail(email);
        if (userOpt.isPresent() && SecurityUtil.isSchoolOwner()) {
            User user = userOpt.get();
            List<SchoolManagement> schoolManagements = user.getSchoolManagements();
            for (SchoolManagement s : schoolManagements) {
                if (Objects.equals(s.getSchool().getId(), schoolId)) {
                    return schoolEnrollDetailService.createEnroll(id, schoolId);
                }
            }
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/enroll/{id}")
    public ResponseEntity<?> listSchoolEnroll(@PathVariable(name = "id") Long id) {
        String email = SecurityUtil.getCurrentAccount();
        Optional<User> userOpt = userService.findUserByEmail(email);

        if (SecurityUtil.isAdmin() || (userOpt.isPresent() && SecurityUtil.isSchoolOwner())) {
            List<SchoolEnrollDetail> listSchoolParent = schoolEnrollDetailService
                    .findAllByUserIdAndStatusTrueAndDeletedFalse(id);
            List<ParentSchoolResponseDTO> parentSchoolResponseDTOS =
                    schoolEnrollDetailMapper.toSchoolDTO(listSchoolParent);

            for (ParentSchoolResponseDTO p : parentSchoolResponseDTOS) {
                List<FeedBack> feedBacks = feedBackService
                        .findFeedbackBySchoolIdAndUserId(p.getSchool().getId(), id);
                p.getSchool().setFeedback(feedBackMapper.toDTO(feedBacks));
            }

            return ResponseEntity.ok(parentSchoolResponseDTOS);
        }

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/noEnroll/{id}")
    public ResponseEntity<?> listSchoolNoEnroll(@PathVariable(name = "id") Long id) {
        String email = SecurityUtil.getCurrentAccount();
        Optional<User> userOpt = userService.findUserByEmail(email);

        if (SecurityUtil.isSchoolOwner() && userOpt.isPresent()) {

            List<SchoolEnrollDetail> listSchoolParent = schoolEnrollDetailService
                    .findAllByUserIdAndStatusTrueAndDeletedFalse(id);

            List<Long> schoolEnrollIdList = listSchoolParent.stream()
                    .map(detail -> detail.getSchool().getId())
                    .toList();

            List<School> schoolAllPublishedList = schoolService.findAllByStatusAndDeletedFalse(SchoolStatusEnum.PUBLISHED);

            List<ParentSchoolResponseDTO> parentSchoolResponseDTOS =
                    schoolEnrollDetailMapper.toSchoolDTO(listSchoolParent);
            return ResponseEntity.ok(parentSchoolResponseDTOS);
        }

        if (SecurityUtil.isAdmin() && userOpt.isPresent()) {
            List<SchoolEnrollDetail> listSchoolParent = schoolEnrollDetailService
                    .findAllByUserIdAndStatusTrueAndDeletedFalse(id);
            List<ParentSchoolResponseDTO> parentSchoolResponseDTOS =
                    schoolEnrollDetailMapper.toSchoolDTO(listSchoolParent);
            return ResponseEntity.ok(parentSchoolResponseDTOS);
        }
        return ResponseEntity.notFound().build();
    }
}
