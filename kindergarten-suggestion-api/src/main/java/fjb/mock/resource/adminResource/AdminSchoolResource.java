package fjb.mock.resource.adminResource;


import fjb.mock.mapper.AdminSchoolMapper;
import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.dto.schoolDTO.SchoolFeedbackDTO;
import fjb.mock.model.dto.schoolDTO.adminSchoolDTO.AdminSchoolRequestDTO;
import fjb.mock.model.entities.School;
import fjb.mock.services.*;
import jakarta.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/api/admin/school")
public class AdminSchoolResource extends BaseResource {
    private final SchoolService schoolService;
    private final AdminSchoolMapper adminSchoolMapper;
    private final AddressService addressService;
    private final ImageEntityService imageEntityService;

    public AdminSchoolResource(SchoolService schoolService,
                               AdminSchoolMapper adminSchoolMapper,
                               UtilitiesService utilitiesService,
                               FacilitiesService facilitiesService,
                               AddressService addressService,
                               ImageEntityService imageEntityService) {
        super(utilitiesService, facilitiesService);
        this.schoolService = schoolService;
        this.adminSchoolMapper = adminSchoolMapper;
        this.addressService = addressService;
        this.imageEntityService = imageEntityService;
    }

    @PatchMapping("/approve/{id}")
    public ResponseEntity<?> approveSchool(@PathVariable Long id) {
        return commonPatch(id, SchoolStatusEnum.APPROVED);
    }

    @PatchMapping("/reject/{id}")
    public ResponseEntity<?> rejectSchool(@PathVariable Long id) {
        return commonPatch(id, SchoolStatusEnum.REJECTED);
    }

    public ResponseEntity<?> commonPatch(Long id, SchoolStatusEnum status) {
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndDeletedFalse(id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        School school = schoolOptional.get();
        school.setStatus(status);
        schoolService.save(school);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(value = "/approve", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> approveSchool(@RequestPart(name = "school") @Valid AdminSchoolRequestDTO schoolRequest,
                                           @RequestPart(name = "mainImage", required = false) MultipartFile mainImage,
                                           @RequestPart(name = "images", required = false) MultipartFile[] files) throws IOException {
        return commonPut(schoolRequest, mainImage, files, SchoolStatusEnum.APPROVED);
    }

    @PutMapping(value = "/reject", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> rejectSchool(@RequestPart(name = "school") @Valid AdminSchoolRequestDTO schoolRequest,
                                          @RequestPart(name = "mainImage", required = false) MultipartFile mainImage,
                                          @RequestPart(name = "images", required = false) MultipartFile[] files) throws IOException {
        return commonPut(schoolRequest, mainImage, files, SchoolStatusEnum.REJECTED);
    }

    @PutMapping(value = "/publish", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> publishEditSchool(@RequestPart(name = "school") @Valid AdminSchoolRequestDTO schoolRequest,
                                               @RequestPart(name = "mainImage", required = false) MultipartFile mainImage,
                                               @RequestPart(name = "images", required = false) MultipartFile[] files) throws IOException {
        return commonPut(schoolRequest, mainImage, files, SchoolStatusEnum.PUBLISHED);
    }

    public ResponseEntity<?> commonPut(AdminSchoolRequestDTO schoolRequest,
                                       MultipartFile mainImage,
                                       MultipartFile[] files, SchoolStatusEnum status) throws IOException {
        Long schoolIdRequest = schoolRequest.getId();
        if (schoolIdRequest == null) {
            return ResponseEntity.notFound().build();
        }

        ResponseEntity<?> validateMainImg = imageEntityService.validateImage(mainImage);
        if (validateMainImg != null) {
            return validateMainImg;
        }
        if (files != null) {
            for (MultipartFile file : files) {
                ResponseEntity<?> validateOtherImg = imageEntityService.validateImage(file);
                if (validateOtherImg != null) {
                    return validateOtherImg;
                }
            }
        }

        Optional<School> oldSchoolOptional = schoolService.findBySchoolIdAndDeletedFalse(schoolIdRequest);
        if (oldSchoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        School oldSchool = oldSchoolOptional.get();
        School school = adminSchoolMapper.toEntity(schoolRequest);

        copyUnchangedProperties(school, oldSchool);

        // Address
        ResponseEntity<?> setAddress = addressService.setAddress(school, schoolRequest);
        if (setAddress != null) {
            return setAddress;
        }

        oldSchool.getSchoolFacilities().forEach(schoolFacilities -> schoolFacilities.setDeleted(true));
        setSchoolFacilities(school, schoolRequest);

        oldSchool.getSchoolUtilities().forEach(schoolUtilities -> schoolUtilities.setDeleted(true));
        setSchoolUtilities(school, schoolRequest);

        // images
        String folderImage = "school";
        school.setImages(imageEntityService.handleUpdateImage(schoolRequest, oldSchool, mainImage, files, folderImage));

        school.setStatus(status);
        school.setDeleted(false);
        schoolService.save(school);
        return ResponseEntity.noContent().build();
    }

}

