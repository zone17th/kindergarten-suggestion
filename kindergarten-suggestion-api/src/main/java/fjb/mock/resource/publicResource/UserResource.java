package fjb.mock.resource.publicResource;

import fjb.mock.component.ErrorResponse;
import fjb.mock.mapper.UserMapper;
import fjb.mock.model.dto.userDTO.FeedBackRequestDTO;
import fjb.mock.model.dto.userDTO.UserProfileDTO;
import fjb.mock.model.entities.FeedBack;
import fjb.mock.model.entities.User;
import fjb.mock.security.SecurityUtil;
import fjb.mock.services.FeedBackService;
import fjb.mock.services.SchoolService;
import fjb.mock.services.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserResource {
    private final UserService userService;

    private final FeedBackService feedBackService;

    private final SchoolService schoolService;
    private final UserMapper userMapper;

    public UserResource(UserService userService, FeedBackService feedBackService, SchoolService schoolService, UserMapper mapper) {
        this.userService = userService;
        this.feedBackService = feedBackService;
        this.schoolService = schoolService;
        this.userMapper = mapper;
    }

    @GetMapping()
    public ResponseEntity<?> showUser() {
        String email = SecurityUtil.getCurrentAccount();
        Optional<User> userOptional = userService.findUserByEmail(email);
        if (userOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        UserProfileDTO userProfileDTO = userMapper.toProfileDTO(userOptional.get());
        return ResponseEntity.ok(userProfileDTO);
    }



    @PutMapping("/update")
    public ResponseEntity<?> update(@Valid @RequestBody UserProfileDTO userProfileDTO) {
        String email = SecurityUtil.getCurrentAccount();
        Optional<User> userOptional = userService.findUserByEmail(email);
        if (userOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        // Nếu thay đổi số điện thoại kiểm tra nó đã tồn tại chưa
        if (!userOptional.get().getPhoneNumber()
                .equals(userProfileDTO.getPhoneNumber())) {
            if (userService.findByPhoneNumber(
                    userProfileDTO.getPhoneNumber()).isPresent()) {
                return ResponseEntity.status(HttpStatus.CONFLICT)
                        .body(new ErrorResponse("Phone number is existed!"));
            }
        }
        User user = userMapper.toEntity(userProfileDTO);
        user.setAccount(userOptional.get().getAccount());
        userService.update(user);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }

    @PostMapping("/feedback")
    public ResponseEntity<?> createFeedBack(
            @Valid @RequestBody FeedBackRequestDTO feedBackRequestDTO
    ) {
        FeedBack feedBack = new FeedBack();
        BeanUtils.copyProperties(feedBackRequestDTO, feedBack);
        double[] numbers = {feedBackRequestDTO.getLearningProgramRating(),
                feedBackRequestDTO.getFacilityAndUtilityRating(),
                feedBackRequestDTO.getExtracurricularActivityRating(),
                feedBackRequestDTO.getTeacherAndStaffRating(),
                feedBackRequestDTO.getHygieneAndNutritionRating()
        };
        feedBack.setAvgRating(Arrays.stream(numbers).average().orElse(0));
        if (schoolService.findBySchoolIdAndDeletedFalse(feedBackRequestDTO.getSchoolId()).isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        feedBack.setSchool(schoolService.findBySchoolIdAndDeletedFalse(feedBackRequestDTO.getSchoolId()).get());
        if (userService.findByID(feedBackRequestDTO.getUserId()).isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        feedBack.setUser(userService.findByID(feedBackRequestDTO.getUserId()).get());
        feedBackService.createNew(feedBack);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
