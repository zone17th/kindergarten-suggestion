package fjb.mock.resource.publicResource;

import fjb.mock.model.appEnum.ChildReceivingAgeEnum;
import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.appEnum.SchoolTypeEnum;

import fjb.mock.model.dto.schoolDTO.SchoolFeedbackDTO;
import fjb.mock.model.dto.userDTO.FeedBackWithUserResponseDTO;
import fjb.mock.model.dto.userDTO.FeedbackResponseDTO;
import fjb.mock.security.SecurityUtil;
import fjb.mock.services.*;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Subquery;
import org.springframework.http.ResponseEntity;

import org.springframework.ui.Model;

import fjb.mock.mapper.SchoolMapper;
import fjb.mock.mapper.FeedBackMapper;

import fjb.mock.model.dto.schoolDTO.HomeFeedBackDTO;
import fjb.mock.model.dto.schoolDTO.SchoolDetailResponseDTO;
import fjb.mock.model.dto.schoolDTO.SchoolEnrollDetailDTO;

import fjb.mock.model.entities.*;

import jakarta.persistence.criteria.Path;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
@RequestMapping("/api/school")
public class SchoolResource {
    private final SchoolService schoolService;
    private final FeedBackService feedBackService;
    private final SchoolMapper schoolMapper;
    private final FeedBackMapper feedBackMapper;
    private final UserService userService;
    private final SchoolEnrollDetailService schoolEnrollDetailService;

    private final Comparator<SchoolDetailResponseDTO> avgRatingDescComparator =
            (o1, o2) -> o2.getAvgRating().compareTo(o1.getAvgRating());

    public SchoolResource(SchoolService schoolService, FeedBackService feedBackService, SchoolMapper schoolMapper, FeedBackMapper feedBackMapper, UserService userService, SchoolEnrollDetailService schoolEnrollDetailService) {
        this.schoolService = schoolService;
        this.feedBackService = feedBackService;
        this.schoolMapper = schoolMapper;
        this.feedBackMapper = feedBackMapper;
        this.userService = userService;
        this.schoolEnrollDetailService = schoolEnrollDetailService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getSchoolDetail(@PathVariable Long id,@RequestParam(required = false)Long userId) {
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndStatusAndDeletedFalse(SchoolStatusEnum.PUBLISHED, id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        School school = schoolOptional.get();
        SchoolDetailResponseDTO schoolDetailResponseDTO = schoolMapper.toDTO(schoolOptional.get());
        schoolDetailResponseDTO.setRate(false);
        Optional<SchoolEnrollDetail> detailOptional = schoolEnrollDetailService.findByUserIdAndSchoolIdAndDeleteFalse(id, userId);
        if (detailOptional.isPresent()) {
            Optional<FeedBack> feedBack = feedBackService.findBySchoolIdAndUserId(id, userId);
            if (feedBack.isEmpty()) {
                schoolDetailResponseDTO.setRate(true);
            }
        }
        schoolDetailResponseDTO.setAvgRating(school.getAvgRating());
        return ResponseEntity.ok(schoolDetailResponseDTO);
    }

    @GetMapping("/{id}/average-feedback")
    public ResponseEntity<?> getSchoolAverageFeedback(@PathVariable Long id) {
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndDeletedFalse(id);

        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        School school = schoolOptional.get();

        double totalLearningProgramRating = 0;
        double totalFacilityAndUtilityRating = 0;
        double totalExtracurricularActivityRating = 0;
        double totalTeacherAndStaffRating = 0;
        double totalHygieneAndNutritionRating = 0;
        SchoolFeedbackDTO schoolFeedbackDTO = new SchoolFeedbackDTO();
        if (school.getFeedBacks() != null) {
            for (FeedBack feedback : school.getFeedBacks()) {
                totalLearningProgramRating += feedback.getLearningProgramRating();
                totalFacilityAndUtilityRating += feedback.getFacilityAndUtilityRating();
                totalExtracurricularActivityRating += feedback.getExtracurricularActivityRating();
                totalTeacherAndStaffRating += feedback.getTeacherAndStaffRating();
                totalHygieneAndNutritionRating += feedback.getHygieneAndNutritionRating();
            }
            int feedbackCount = school.getFeedBacks().size();
            schoolFeedbackDTO.setAvgLearningProgramRating((double) Math.round(totalLearningProgramRating / feedbackCount));
            schoolFeedbackDTO.setAvgFacilityAndUtilityRating((double) Math.round(totalFacilityAndUtilityRating / feedbackCount));
            schoolFeedbackDTO.setAvgExtracurricularActivityRating((double) Math.round(totalExtracurricularActivityRating / feedbackCount));
            schoolFeedbackDTO.setAvgTeacherAndStaffRating((double) Math.round(totalTeacherAndStaffRating / feedbackCount));
            schoolFeedbackDTO.setAvgHygieneAndNutritionRating((double) Math.round(totalHygieneAndNutritionRating / feedbackCount));
            schoolFeedbackDTO.setFeedbackCount(feedbackCount);
        } else {
            schoolFeedbackDTO.setAvgLearningProgramRating(totalLearningProgramRating);
            schoolFeedbackDTO.setAvgFacilityAndUtilityRating(totalFacilityAndUtilityRating);
            schoolFeedbackDTO.setAvgExtracurricularActivityRating(totalExtracurricularActivityRating);
            schoolFeedbackDTO.setAvgTeacherAndStaffRating(totalTeacherAndStaffRating);
            schoolFeedbackDTO.setAvgHygieneAndNutritionRating(totalHygieneAndNutritionRating);
            schoolFeedbackDTO.setFeedbackCount(0);
        }
        schoolFeedbackDTO.setAvgRating(school.getAvgRating());

        return ResponseEntity.ok(schoolFeedbackDTO);
    }

    @GetMapping("/search")
    public Page<SchoolDetailResponseDTO> searchSchool(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "10") Integer size,
            @RequestParam(required = false, name = "name") Optional<String> keyWord,
            @RequestParam(required = false, name = "city") Optional<Long> city,
            @RequestParam(required = false, name = "district") Optional<Long> district,
            @RequestParam(required = false, name = "type") Optional<String> schoolTypes,
            @RequestParam(required = false, name = "age") Optional<String> admissionAge,
            @RequestParam(required = false, name = "minFee") Optional<BigInteger> minFee,
            @RequestParam(required = false, name = "maxFee") Optional<BigInteger> maxFee,
            @RequestParam(required = false, name = "facilities") List<String> facilities,
            @RequestParam(required = false, name = "rating") Optional<Double> rating,
            @RequestParam(required = false, name = "utilities") List<String> utilities,
            @RequestParam(required = false, name = "sort") Integer sortBy,
            Model model) {

        List<Specification<School>> specs = new ArrayList<>();

        Specification<School> nameSpec = null;
        if (keyWord.isPresent() && !keyWord.get().isEmpty()) {
            String keyword = keyWord.get();
            nameSpec = (root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.like(root.get("name"), "%" + keyword + "%"));
            specs.add(nameSpec);
        }

        Specification<School> locationSpec = null;
        if (city.isPresent()) {
            Long cityId = city.get();
            locationSpec = (root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.equal(root.get("city").get("id"), cityId));
            specs.add(locationSpec);
        }

        Specification<School> districtSpec = null;
        if (district.isPresent()) {
            Long districtId = district.get();
            districtSpec = (root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.equal(root.get("district").get("id"), districtId));
            specs.add(districtSpec);
        }

        // filter
        Specification<School> schoolTypeSpec = null;
        if (schoolTypes.isPresent() && !schoolTypes.get().isEmpty()) {
            String schoolTypeSelect = schoolTypes.get();
            SchoolTypeEnum schoolTypeEnum = SchoolTypeEnum.valueOf(schoolTypeSelect);
            schoolTypeSpec = ((root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.equal(root.get("type"), schoolTypeEnum)));
            specs.add(schoolTypeSpec);
        }

        Specification<School> ageSpec = null;
        if (admissionAge.isPresent() && !admissionAge.get().isEmpty()) {
            String ageSelect = admissionAge.get();
            ChildReceivingAgeEnum ageEnum = ChildReceivingAgeEnum.valueOf(ageSelect);
            ageSpec = ((root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.equal(root.get("childReceivingAge"), ageEnum)));
            specs.add(ageSpec);
        }

        Specification<School> minFeeSpec = null;
        if (minFee.isPresent()) {
            BigInteger minFeeSelect = minFee.get();

            minFeeSpec = ((root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.greaterThanOrEqualTo(root.get("minFee"), minFeeSelect)));
            specs.add(minFeeSpec);
        }

        Specification<School> maxFeeSpec = null;
        if (maxFee.isPresent()) {
            BigInteger maxFeeSelect = maxFee.get();
            maxFeeSpec = ((root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.lessThanOrEqualTo(root.get("maxFee"), maxFeeSelect)));
            specs.add(maxFeeSpec);
        }

        Specification<School> facilitiesSpec = null;
        if (facilities != null && !facilities.isEmpty()) {
            facilitiesSpec = (root, query, criteriaBuilder) -> {
                Subquery<Integer> schoolIdSubquery = query.subquery(Integer.class);
                Root<SchoolFacilities> schoolFacilitiesRoot = schoolIdSubquery.from(SchoolFacilities.class);
                Path<Integer> schoolId = schoolFacilitiesRoot.get("school").get("id");
                Path<Integer> facilityId = schoolFacilitiesRoot.get("facility").get("id");

                Predicate undeletedCondition = criteriaBuilder.equal(schoolFacilitiesRoot.get("deleted"), false);

                schoolIdSubquery.select(schoolId)
                        .where(
                                facilityId.in(facilities),
                                undeletedCondition
                        )
                        .groupBy(schoolId)
                        .having(criteriaBuilder.equal(criteriaBuilder.count(facilityId), facilities.size()));
                return criteriaBuilder.or(root.get("id").in(schoolIdSubquery));

//                schoolIdSubquery.select(schoolId)
//                        .where(facilityId.in(facilities)).groupBy(schoolId)
//                        .having(criteriaBuilder.equal(criteriaBuilder.count(facilityId), facilities.size()));
//                return criteriaBuilder.or(root.get("id").in(schoolIdSubquery));
            };
            specs.add(facilitiesSpec);
        }

        Specification<School> utilitiesSpec = null;
        if (utilities != null && !utilities.isEmpty()) {
            utilitiesSpec = (root, query, criteriaBuilder) -> {
                Subquery<Integer> schoolIdSubquery = query.subquery(Integer.class);
                Root<SchoolUtilities> schoolUtilitiesRoot = schoolIdSubquery.from(SchoolUtilities.class);
                Path<Integer> schoolId = schoolUtilitiesRoot.get("school").get("id");
                Path<Integer> utilityId = schoolUtilitiesRoot.get("utility").get("id");

                Predicate undeletedCondition = criteriaBuilder.equal(schoolUtilitiesRoot.get("deleted"), false);

                schoolIdSubquery.select(schoolId)
                        .where(
                                utilityId.in(utilities),
                                undeletedCondition
                        )
                        .groupBy(schoolId)
                        .having(criteriaBuilder.equal(criteriaBuilder.count(utilityId), utilities.size()));
                return criteriaBuilder.or(root.get("id").in(schoolIdSubquery));


//                schoolIdSubquery.select(schoolId)
//                        .where(utilityId.in(utilities)).groupBy(schoolId)
//                        .having(criteriaBuilder.equal(criteriaBuilder.count(utilityId), utilities.size()));
//                return criteriaBuilder.or(root.get("id").in(schoolIdSubquery));
            };
            specs.add(utilitiesSpec);
        }

        Specification<School> rateSpec = null;
        if (rating.isPresent()) {
            Double ratingSelect = rating.get();
            rateSpec= (root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.greaterThanOrEqualTo(root.get("avgRating"),  ratingSelect ));
            specs.add(rateSpec);
        }

        // Sort by for 4 case
        Sort sort = null;
        if (sortBy != null) {
            sort = switch (sortBy) {
                case 1 -> Sort.by("avgRating").descending(); // Rating từ cao về thấp
                case 2 -> Sort.by("createdDate").descending(); //created date từ mới nhất
                case 3 -> Sort.by("minFee").descending(); // minFee Giảm dần
                case 4 -> Sort.by("minFee").ascending();// minFee tăng dần
                default -> Sort.by("avgRating").descending();
            };
        } else {
            sort = Sort.by("avgRating").descending();
        }

        Specification<School> combinedSpec = specs.stream().reduce(Specification::and).orElse(null);
        PageRequest pageRequest = PageRequest.of(page, size, sort);

        Page<School> schools = schoolService.getPageData(combinedSpec, pageRequest);
        Page<SchoolDetailResponseDTO> result = schools.map(school -> {
            SchoolDetailResponseDTO schoolResponse = schoolMapper.toDTO(school);
            Optional<ImageEntity> imageEntityOptional = school.getImages().stream()
                    .filter(ImageEntity::getIsMainImage).findFirst();
            if (imageEntityOptional.isEmpty()) {
                schoolResponse
                        .setMainImage("https://kindergartenchaos.com/wp-content/uploads/2015/08/Kindergarten-Classroom-Reveal-kindergartenchaos.com_.jpg");
            } else {
                schoolResponse.setMainImage(imageEntityOptional.get().getFilePath());
            }
            return schoolResponse;
        });
        int currentPage = page;
        model.addAttribute("currentPage", currentPage);
        return result;
    }


    @GetMapping("/enrolled")
    public ResponseEntity<?> getListSchoolEnrolled(@RequestParam(defaultValue = "0") Integer page,
                                                   @RequestParam(defaultValue = "3") Integer size,
                                                   @RequestParam Boolean status) {
        String email = SecurityUtil.getCurrentAccount();
        Optional<User> userOptional = userService.findUserByEmail(email);
        if (userOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        Long userId = userOptional.get().getId();
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdDate"));
        Page<SchoolEnrollDetail> schoolUserDetails = schoolEnrollDetailService
                .findAllByUserIdAndIsEnrollTrueAndStatusAndDeletedFalse(userId, status, pageable);
        Page<SchoolEnrollDetailDTO> result = schoolUserDetails.map(school -> {
            SchoolEnrollDetailDTO schoolResponse = schoolMapper.toDTO(school);
            Optional<ImageEntity> imageEntityOptional = school.getSchool().getImages().stream()
                    .filter(ImageEntity::getIsMainImage).findFirst();
            if (imageEntityOptional.isEmpty()) {
                schoolResponse.getSchool()
                        .setMainImage(
                                "https://kindergartenchaos.com/wp-content/uploads/2015/08/Kindergarten-Classroom-Reveal-kindergartenchaos.com_.jpg");
            } else {
                schoolResponse.getSchool().setMainImage(imageEntityOptional.get().getFilePath());
            }

            Integer totalFeedbacks = school.getSchool().getFeedBacks().size();
            schoolResponse.getSchool().setTotalFeedbacks(totalFeedbacks);

            Optional<FeedBack> feedBackOptional = school.getSchool().getFeedBacks()
                    .stream()
                    .filter(fb -> Objects.equals(fb.getUser().getId(), userId)).findFirst();
            if (feedBackOptional.isPresent()) {
                FeedBack feedBack = feedBackOptional.get();
                FeedbackResponseDTO feedbackDTO = feedBackMapper.toDTO(feedBack);
                schoolResponse.getSchool().setFeedback(feedbackDTO);
            }
            return schoolResponse;
        });
        return ResponseEntity.ok(result);
    }

    @GetMapping
    public ResponseEntity<?> selectTopNFeedBack() {

        List<FeedBack> feedBackList = schoolService.getTopNFeedBack();
        if (feedBackList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        List<HomeFeedBackDTO> homeFeedBackDTOList = new ArrayList<>();
        for (FeedBack feedBack : feedBackList) {
            HomeFeedBackDTO homeFeedBackDTO = new HomeFeedBackDTO();
            homeFeedBackDTO.setFullName(feedBack.getUser().getFullName());
            homeFeedBackDTO.setImageProfile(feedBack.getUser().getImageProfile());
            homeFeedBackDTO.setContent(feedBack.getContent());
            homeFeedBackDTO.setAvgRating(feedBack.getAvgRating());
            homeFeedBackDTOList.add(homeFeedBackDTO);
        }
        return ResponseEntity.ok(homeFeedBackDTOList);
    }

    @GetMapping("/min-fee")
    public ResponseEntity<?> findMinFee() {
        BigInteger minFee = schoolService.findMinFee();
        return ResponseEntity.ok(Objects.requireNonNullElse(minFee, 0));
    }

    @GetMapping("/max-fee")
    public ResponseEntity<?> findMaxFee() {
        BigInteger maxFee = schoolService.findMaxFee();
        return ResponseEntity.ok(Objects.requireNonNullElse(maxFee, 0));
    }


    @GetMapping("/{id}/feedbacks2")
    public ResponseEntity<?> getAllFeedbacksSchool2(@PathVariable Long id,
                                                   @RequestParam(defaultValue = "0") Integer page,
                                                   @RequestParam(defaultValue = "3") Integer size,
                                                   @RequestParam(required = false, name = "sort") Integer sortBy) {
//        Specification<FeedBack> spec = null;

        Pageable pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdDate"));

        Page<FeedBack> feedBacks = feedBackService.findAllBySchoolIdAndDeletedFalse(id, pageRequest);
        Page<FeedBackWithUserResponseDTO> result = feedBacks
                .map(feedBackMapper::toDTOs);
        return ResponseEntity.ok(result);
    }
    @GetMapping("/{id}/feedbacks")
    public ResponseEntity<?> getAllFeedbacksSchool(@PathVariable Long id,
                                                   @RequestParam(defaultValue = "0") Integer page,
                                                   @RequestParam(defaultValue = "3") Integer size,
                                                   @RequestParam(required = false, name = "start") Optional<String>  startDate,
                                                   @RequestParam(required = false, name = "end") Optional<String>  endDate,
                                                   @RequestParam(required = false, name = "rating") Optional<Double> rating,
                                                   @RequestParam(required = false, name = "sort") Integer sortBy) {
//        Specification<FeedBack> spec = null;

        List<Specification<FeedBack>> specs = new ArrayList<>();

        Specification<FeedBack> startSpec = null;
        if (startDate.isPresent() && startDate.get() != "") {
            String startDateSelect = startDate.get();
            LocalDate startDateAsLocalDate = LocalDate.parse(startDateSelect);

            startSpec = ((root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.greaterThanOrEqualTo(root.get("createdDate"), startDateAsLocalDate)));
            specs.add(startSpec);
        }

        Specification<FeedBack> endSpec = null;
        if (endDate.isPresent() && startDate.get() != "") {
            String endDateSelect = endDate.get();

            LocalDate endDateAsLocalDate = LocalDate.parse(endDateSelect);

            endSpec = ((root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.lessThanOrEqualTo(root.get("createdDate"), endDateAsLocalDate)));
            specs.add(endSpec);
        }

        Specification<FeedBack> rateSpec = null;
        if (rating.isPresent()) {
            Double ratingSelect = rating.get();
            rateSpec = (root, query, criteriaBuilder) -> criteriaBuilder
                    .or(criteriaBuilder.greaterThanOrEqualTo(root.get("avgRating"), ratingSelect));
            specs.add(rateSpec);
        }

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdDate"));
        Specification<FeedBack> spec = specs.stream().reduce(Specification::and).orElse(null);
        Page<FeedBack> feedBacks = feedBackService.findAll(id,spec, pageRequest);
        Page<FeedBackWithUserResponseDTO> result = feedBacks
                .map(feedBackMapper::toDTOs);
        return ResponseEntity.ok(result);
    }
}
