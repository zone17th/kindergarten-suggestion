package fjb.mock.resource.publicResource;

import fjb.mock.exception.UnAuthorizedException;
import fjb.mock.mapper.RequestCounsellingMapper;
import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.appEnum.RequestStatusEnum;
import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.dto.userDTO.RequestCounsellingDTO;
import fjb.mock.model.dto.userDTO.RequestCounsellingResponseDTO;
import fjb.mock.model.entities.*;
import fjb.mock.security.SecurityUtil;
import fjb.mock.services.RequestCounsellingService;
import fjb.mock.services.SchoolService;
import fjb.mock.services.UserService;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/request_counselling")
public class RequestCounsellingResource {
    private final RequestCounsellingService requestCounsellingService;
    private final UserService userService;
    private final SchoolService schoolService;
    private final RequestCounsellingMapper requestCounsellingMapper;

    public RequestCounsellingResource(RequestCounsellingService requestCounsellingService, UserService userService, SchoolService schoolService, RequestCounsellingMapper requestCounsellingMapper) {
        this.requestCounsellingService = requestCounsellingService;
        this.userService = userService;
        this.schoolService = schoolService;
        this.requestCounsellingMapper = requestCounsellingMapper;
    }

    @GetMapping
    public ResponseEntity<?> getAllRequestById(@RequestParam(required = false, defaultValue = "0") Integer page,
                                               @RequestParam(required = false, defaultValue = "3") Integer size
    ) {
        Sort sort = Sort.by(Sort.Direction.DESC, "status")
                .and(Sort.by(Sort.Direction.ASC, "createdDate"));
        PageRequest pageRequest = PageRequest.of(page, size, sort);
        String email = SecurityUtil.getCurrentAccount();
        if (email.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        Page<RequestCounselling> counsellingPage = requestCounsellingService.getPageData(email, pageRequest);
        if (counsellingPage.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        Page<RequestCounsellingResponseDTO> dtoPage = counsellingPage.map(requestCounsellingMapper::toDTO);
        for (int i = 0; i < dtoPage.getContent().size(); i++) {
            List<FeedBack> feedBackList =
                    counsellingPage.getContent().get(i).getSchool().getFeedBacks();
            Double avgRate =
                    feedBackList.stream().mapToDouble(FeedBack::getAvgRating).average().orElse(0.0);

            dtoPage.getContent().get(i).getSchool().setAvgRating(avgRate);
            dtoPage.getContent().get(i).getSchool().setTotalFeedbacks(feedBackList.size());
        }

        return ResponseEntity.ok().body(dtoPage);
    }

    @PostMapping
    public ResponseEntity<?> createNew(@Valid @RequestBody RequestCounsellingDTO requestCounsellingDTO) {
        try {
            RequestCounselling request = new RequestCounselling();
            String email = SecurityUtil.getCurrentAccount();
            Optional<User> user = userService.findUserByEmail(email);
            Optional<School> school = schoolService.findBySchoolIdAndDeletedFalse(requestCounsellingDTO.getSchoolId());

            if (user.isEmpty() || school.isEmpty()) {
                return ResponseEntity.noContent().build();
            }

            BeanUtils.copyProperties(requestCounsellingDTO, request);
            request.setUser(user.get());
            request.setSchool(school.get());
            requestCounsellingService.create(request);
            RequestCounsellingResponseDTO responseDTO = requestCounsellingMapper.toDTO(request);
            return ResponseEntity.status(HttpStatus.CREATED).body(responseDTO);
        } catch (UnAuthorizedException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

    }

    @PutMapping("/cancel/{id}")
    public ResponseEntity<?> cancelRequest(@PathVariable(name = "id") Long id) {
        Optional<RequestCounselling> requestOtp = requestCounsellingService.findById(id);
        if (requestOtp.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        RequestCounselling request = requestOtp.get();
        requestCounsellingService.cancelRequest(request);
        return ResponseEntity.ok("Successfully!");
    }

//    @GetMapping("/search")
//    public ResponseEntity<?> getPageData(@RequestParam(name = "n") Optional<String> nameOtp,
//                                         @RequestParam(name = "p") Optional<String> phoneOtp,
//                                         @RequestParam(name = "e") Optional<String> mailOtp,
//                                         @RequestParam(name = "s") Optional<String> statusOtp,
//                                         @RequestParam(name = "sort") Optional<String> sortOtp,
//                                         @RequestParam(required = false, defaultValue = "5") Integer size,
//                                         @RequestParam(required = false, defaultValue = "0") Integer page,
//                                         Sort sort) {
//        List<Specification<RequestCounselling>> specs = new ArrayList<>();
//        Specification<RequestCounselling> spec = null;
//        if (nameOtp.isPresent()) {
//            String name = nameOtp.get();
//            spec =
//                    ((root, query, criteriaBuilder) -> criteriaBuilder.or(
//                            criteriaBuilder.like(root.get("fullName"), "%" + name + "%"))
//
//                    );
//            specs.add(spec);
//        }
//        if (phoneOtp.isPresent()) {
//            String phone = phoneOtp.get();
//            spec =
//                    ((root, query, criteriaBuilder) -> criteriaBuilder.or(
//                            criteriaBuilder.like(root.get("phoneNumber"), "%" + phone + "%"))
//
//                    );
//            specs.add(spec);
//        }
//        if (mailOtp.isPresent()) {
//            String mail = mailOtp.get();
//            spec =
//                    ((root, query, criteriaBuilder) -> criteriaBuilder.or(
//                            criteriaBuilder.like(root.get("email"), "%" + mail + "%"))
//
//                    );
//            specs.add(spec);
//        }
//        if (statusOtp.isPresent()) {
//            RequestStatusEnum status = RequestStatusEnum.valueOf(statusOtp.get().toUpperCase());
//            spec = ((root, query, criteriaBuilder) ->
//                    criteriaBuilder.equal(root.get("status"), status)
//            );
//            specs.add(spec);
//        }
//        spec = specs.stream().reduce(Specification::and).orElse(null);
//
//        //sort
//        if (sortOtp.isPresent()) {
//            if (sortOtp.get().equals("1")) {
//                sort = Sort.by(Sort.Direction.ASC, "createdDate");
//            }
//        }
//
//        PageRequest pageRequest = PageRequest.of(page, size, sort);
//        Page<RequestCounselling> counsellingPage = requestCounsellingService.getPageData(spec, pageRequest);
//        Page<RequestCounsellingResponseDTO> dtoPage = counsellingPage.map(requestCounsellingMapper::toDTO);
//        for (
//                int i = 0; i < dtoPage.getContent().
//
//                size();
//
//                i++) {
//            List<FeedBack> feedBackList =
//                    counsellingPage.getContent().get(i).getSchool().getFeedBacks();
//            Double avgRate =
//                    feedBackList.stream().mapToDouble(FeedBack::getAvgRating).average().orElse(0.0);
//
//            dtoPage.getContent().get(i).getSchool().setAvgRating(avgRate);
//            dtoPage.getContent().get(i).getSchool().setTotalFeedbacks(feedBackList.size());
//        }
//
//        return ResponseEntity.ok(dtoPage);
//    }

    @GetMapping("/search")
    public ResponseEntity<?> getPageData(@RequestParam(name = "n") Optional<String> nameOtp,
                                         @RequestParam(name = "sort") Optional<String> sortOtp,
                                         @RequestParam(required = false, defaultValue = "5") Integer size,
                                         @RequestParam(required = false, defaultValue = "0") Integer page,
                                         Sort sort) {
        List<Specification<RequestCounselling>> specs = new ArrayList<>();
        Specification<RequestCounselling> spec = null;
        if (nameOtp.isPresent()) {
            String name = nameOtp.get();
            List<RequestStatusEnum> enums = Arrays.asList(RequestStatusEnum.values());
            boolean isRole2 = enums.stream().anyMatch(r->r.name().equalsIgnoreCase(name));
            if (isRole2) {
                spec = ((root, query, criteriaBuilder) -> criteriaBuilder.or(
                        criteriaBuilder.equal(root.get("status"),RequestStatusEnum.valueOf(name.toUpperCase()))));
            } else {
            spec =
                    ((root, query, criteriaBuilder) -> criteriaBuilder.or(
                            criteriaBuilder.like(root.get("fullName"), "%" + name + "%"),
                            criteriaBuilder.like(root.get("phoneNumber"), "%" + name + "%"),
                            criteriaBuilder.like(root.get("createdDate"), "%" + name + "%"))
                    );
            }
        }
        specs.add(spec);
        UserRoleEnum userRoleEnum = SecurityUtil.getRoleCurrentAccount();
        if (userRoleEnum.name().equals("SCHOOL_OWNER")) {
            Specification<FeedBack> spec1 = ((root, query, criteriaBuilder) -> {
                Join<User,SchoolManagement> userSchoolJoin = root.join(SchoolManagement_.USER,JoinType.INNER);
                Join<SchoolManagement,School> schoolManagementSchoolJoin = userSchoolJoin.join(SchoolManagement_.SCHOOL,JoinType.INNER);
                Join<School,FeedBack> schoolFeedBackJoin = schoolManagementSchoolJoin.join(School_.FEED_BACKS,JoinType.INNER);
                return null;
            });
        }


        //sort
        if (sortOtp.isPresent()) {
            if (sortOtp.get().equals("1")) {
                sort = Sort.by(Sort.Direction.ASC, "createdDate");
            }
        }

        PageRequest pageRequest = PageRequest.of(page, size, sort);
        Page<RequestCounselling> counsellingPage = requestCounsellingService.getPageData(spec, pageRequest);
        Page<RequestCounsellingResponseDTO> dtoPage = counsellingPage.map(requestCounsellingMapper::toDTO);
        for (
                int i = 0; i < dtoPage.getContent().

                size();

                i++) {
            List<FeedBack> feedBackList =
                    counsellingPage.getContent().get(i).getSchool().getFeedBacks();
            Double avgRate =
                    feedBackList.stream().mapToDouble(FeedBack::getAvgRating).average().orElse(0.0);

            dtoPage.getContent().get(i).getSchool().setAvgRating(avgRate);
            dtoPage.getContent().get(i).getSchool().setTotalFeedbacks(feedBackList.size());
        }

        return ResponseEntity.ok(dtoPage);
    }

}
