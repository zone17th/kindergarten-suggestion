package fjb.mock.resource.publicResource;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import fjb.mock.constant.EmailContextConstant;
import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.dto.userDTO.MailRequestDTO;
import fjb.mock.model.entities.Account;
import fjb.mock.model.entities.School;
import fjb.mock.model.entities.User;
import fjb.mock.security.TokenProvider;
import fjb.mock.services.AccountService;
import fjb.mock.services.MailService;
import fjb.mock.services.SchoolService;
import fjb.mock.services.UserService;
import jakarta.mail.MessagingException;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/mail")
public class MailResource {
    private final MailService mailService;
    private final UserService userService;
    private final TokenProvider tokenProvider;
    private final AccountService accountService;
    private final SchoolService schoolService;

    @Autowired
    private TemplateEngine templateEngine;

    @Value("${spring.web.public.host}")
    private String publicHost;

    @Value("${spring.web.private.host}")
    private String privateHost;


    public MailResource(MailService mailService, UserService userService, TokenProvider tokenProvider,
                        AuthenticationManagerBuilder authenticationManagerBuilder, AccountService accountService, SchoolService schoolService) {
        this.mailService = mailService;
        this.userService = userService;
        this.tokenProvider = tokenProvider;
        this.accountService = accountService;
        this.schoolService = schoolService;
    }

    @PostMapping("/request")
    public ResponseEntity<?> sendMail(@RequestBody @Valid MailRequestDTO mailRequestDTO) {
        try {
            Context context = new Context();
            context.setVariable("name", mailRequestDTO.getFullName());
            context.setVariable("dateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            String emailContext = templateEngine.process("mail/requestMailContext",context);
            mailService.sendMail(mailRequestDTO.getEmail(),
                    EmailContextConstant.REQUEST_SUBJECT,
                    emailContext);
            return ResponseEntity.ok("Send email success");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.noContent().build();
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> confirmRegisterMail(@RequestBody @Valid MailRequestDTO mailRequestDTO)
            throws MessagingException {
        Optional<Account> accountOptional = accountService
                .findByEmailAndActiveFalseAndDeletedFalse(mailRequestDTO.getEmail());
        if (accountOptional.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        Optional<User> optionalUser = userService.findUserByEmail(mailRequestDTO.getEmail());
        if (optionalUser.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        String activeToken = tokenProvider.generateActiveToken(mailRequestDTO.getEmail());
        String link = publicHost+"/auth/account/active/"+activeToken;
        Context context = new Context();
        context.setVariable("name", mailRequestDTO.getFullName());
        context.setVariable("link",link);
        context.setVariable("dateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String emailContext = templateEngine.process("mail/registerMailContext",context);
        mailService.sendMail(mailRequestDTO.getEmail(),
                EmailContextConstant.REGISTER_SUBJECT,
                emailContext);
        return ResponseEntity.ok(mailRequestDTO);
    }

    @PostMapping("/password")
    public ResponseEntity<?> resetPasswordMail(@RequestBody @Valid MailRequestDTO mailRequestDTO)
            throws MessagingException {
        Optional<Account> accountOptional = accountService
                .findByEmailAndActiveTrueAndDeletedFalse(mailRequestDTO.getEmail());
        if (accountOptional.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        Optional<User> optionalUser = userService.findUserByEmail(mailRequestDTO.getEmail());;
        if (optionalUser.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        String resetToken = tokenProvider.generateResetToken(mailRequestDTO.getEmail());
        String link;
        if (accountOptional.get().getRole().equals(UserRoleEnum.PARENTS)) {
            link =publicHost+"/auth/account/reset-password/"+resetToken;
        }
        else {
            link =privateHost+"/admin/auth/reset-password/"+resetToken;
        }
        Context context = new Context();
        context.setVariable("name", optionalUser.get().getFullName());
        context.setVariable("email",mailRequestDTO.getEmail());
        context.setVariable("link",link);
        context.setVariable("dateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String emailContext = templateEngine.process("mail/resetPasswordMailContext",context);

        mailService.sendMail(mailRequestDTO.getEmail(),
                EmailContextConstant.RESET_SUBJECT,
                emailContext);
        return ResponseEntity.ok(mailRequestDTO);
    }
    @PostMapping("/newUser")
    public ResponseEntity<?> createNewUserMail(@RequestBody @Valid MailRequestDTO mailRequestDTO) throws MessagingException {
        Optional<Account> accountOptional = accountService
                .findByEmailAndActiveTrueAndDeletedFalse(mailRequestDTO.getEmail());
        if (accountOptional.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        Optional<User> optionalUser = userService.findUserByEmail(mailRequestDTO.getEmail());
        if (optionalUser.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        String adminMail = accountService.findAllByRole(UserRoleEnum.ADMIN).get().getEmail();
        String resetToken = tokenProvider.generateResetToken(mailRequestDTO.getEmail());
        String link;
        if (accountOptional.get().getRole().equals(UserRoleEnum.PARENTS)) {
            link =publicHost+"/auth/account/reset-password/"+resetToken;
        }
        else {
            link =privateHost+"/admin/auth/reset-password/"+resetToken;
        }
        Context context = new Context();
        context.setVariable("name", optionalUser.get().getFullName());
        context.setVariable("email",mailRequestDTO.getEmail());
        context.setVariable("link",link);
        context.setVariable("adminMail",adminMail);
        context.setVariable("dateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String emailContext = templateEngine.process("mail/createNewUserMailContext",context);

        mailService.sendMail(mailRequestDTO.getEmail(),
                EmailContextConstant.CREATE_USER_SUBJECT,
                emailContext);
        return ResponseEntity.ok(mailRequestDTO);
    }
    @PostMapping("/school-submit/{id}")
    public ResponseEntity<?> submitSchoolMail(@PathVariable Long id) throws MessagingException {
        if (id == null) {
            return ResponseEntity.badRequest().body("School ID is required.");
        }
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndStatusAndDeletedFalse(SchoolStatusEnum.SUBMITTED, id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        String schoolName = schoolOptional.get().getName();
        String link= privateHost+"/admin/school/"+id;
        Context context = new Context();
        context.setVariable("schoolName", schoolOptional.get().getName());
        context.setVariable("link",link);
        context.setVariable("dateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String emailContext = templateEngine.process("mail/submitSchoolMailContext",context);
        String adminMail = accountService.findAllByRole(UserRoleEnum.ADMIN).get().getEmail();
        mailService.sendMail(adminMail,
                EmailContextConstant.SCHOOL_SUBMIT_SUBJECT,
                emailContext);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @PostMapping("/school-approve/{id}")
    public ResponseEntity<?> approveSchoolMail(@PathVariable Long id) throws MessagingException {
        if (id == null) {
            return ResponseEntity.badRequest().body("School ID is required.");
        }
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndStatusAndDeletedFalse(SchoolStatusEnum.APPROVED, id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        String link= privateHost+"/admin/school/"+id;
        String schoolOwnerMail = schoolOptional.get().getSchoolManagement().getUser().getAccount().getEmail();
        Context context = new Context();
        context.setVariable("userName",schoolOptional.get().getSchoolManagement().getUser().getFullName());
        context.setVariable("schoolName", schoolOptional.get().getName());
        context.setVariable("link",link);
        context.setVariable("dateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String emailContext = templateEngine.process("mail/approveSchoolMailContext",context);
        mailService.sendMail(schoolOwnerMail,
                EmailContextConstant.SCHOOL_APPROVE_SUBJECT,
                emailContext);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @PostMapping("/school-reject/{id}")
    public ResponseEntity<?> rejectSchoolMail(@PathVariable Long id) throws MessagingException {
        if (id == null) {
            return ResponseEntity.badRequest().body("School ID is required.");
        }
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndStatusAndDeletedFalse(SchoolStatusEnum.REJECTED, id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        String adminMail = accountService.findAllByRole(UserRoleEnum.ADMIN).get().getEmail();
        String schoolOwnerMail = schoolOptional.get().getSchoolManagement().getUser().getAccount().getEmail();
        Context context = new Context();
        context.setVariable("userName",schoolOptional.get().getSchoolManagement().getUser().getFullName());
        context.setVariable("schoolName", schoolOptional.get().getName());
        context.setVariable("adminMail",adminMail);
        context.setVariable("dateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String emailContext = templateEngine.process("mail/rejectSchoolMailContext",context);
        mailService.sendMail(schoolOwnerMail,
                EmailContextConstant.SCHOOL_REJECT_SUBJECT,
                emailContext);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @PostMapping("/school-publish/{id}")
    public ResponseEntity<?> publishSchoolMail(@PathVariable Long id) throws MessagingException {
        if (id == null) {
            return ResponseEntity.badRequest().body("School ID is required.");
        }
        Optional<School> schoolOptional = schoolService.findBySchoolIdAndStatusAndDeletedFalse(SchoolStatusEnum.PUBLISHED, id);
        if (schoolOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        String link= privateHost+"/admin/school/"+id;
        String schoolOwnerMail = schoolOptional.get().getSchoolManagement().getUser().getAccount().getEmail();
        String adminMail = accountService.findAllByRole(UserRoleEnum.ADMIN).get().getEmail();
        Context context = new Context();
        context.setVariable("userName",schoolOptional.get().getSchoolManagement().getUser().getFullName());
        context.setVariable("schoolName", schoolOptional.get().getName());
        context.setVariable("adminMail",adminMail);
        context.setVariable("link",link);

        context.setVariable("dateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String emailContext = templateEngine.process("mail/publishSchoolMailContext",context);
        mailService.sendMail(schoolOwnerMail,
                EmailContextConstant.SCHOOL_PUBLISH_SUBJECT,
                emailContext);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
