package fjb.mock.resource.publicResource;

import java.util.Optional;

import fjb.mock.mapper.UserMapper;
import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.security.TokenProvider;
import fjb.mock.services.AccountService;
import fjb.mock.services.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fjb.mock.component.SuccessResponse;
import fjb.mock.model.dto.userDTO.LoginRequestDTO;
import fjb.mock.model.dto.userDTO.LoginResponseDTO;
import fjb.mock.model.dto.userDTO.RegisterRequestDTO;
import fjb.mock.model.entities.Account;
import fjb.mock.model.entities.User;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationResource {
    private final TokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final UserService userService;

    private final AccountService accountService;
    private final UserMapper userMapper;

    public AuthenticationResource(TokenProvider tokenProvider,
                                  AuthenticationManagerBuilder authenticationManagerBuilder, UserService userService,
                                  AccountService accountService, UserMapper userMapper) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userService = userService;
        this.accountService = accountService;
        this.userMapper = userMapper;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid LoginRequestDTO loginRequestDTO) {
        Optional<Account> accountOptional = accountService
                .findByEmailAndActiveTrueAndDeletedFalse(loginRequestDTO.getEmail());
        // Nếu không tồn tại thông báo lỗi
        if (accountOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        LoginResponseDTO loginResponseDTO = accountService.login(loginRequestDTO.getEmail(),
                loginRequestDTO.getPassword());
        if (loginResponseDTO == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok(loginResponseDTO);
    }


    @PostMapping("/logout")
    public ResponseEntity<?> logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
            SecurityContextHolder.clearContext();
        }
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody @Valid RegisterRequestDTO registerRequestDTO,
                                      BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        if (accountService.checkExistEmail(registerRequestDTO.getEmail())) {
            return ResponseEntity.badRequest().body("Email is existed");
        }
        if (accountService.checkExistPhone(registerRequestDTO.getPhoneNumber())) {
            return ResponseEntity.badRequest().body("Phone Number is existed");
        }

        User user = new User();
        user.setFullName(registerRequestDTO.getFullName());
        user.setPhoneNumber(registerRequestDTO.getPhoneNumber());
        Account account = new Account();
        account.setEmail(registerRequestDTO.getEmail());
        account.setPassword(registerRequestDTO.getPassword());
        account.setActive(false);

        accountService.createNew(account, user);
        return ResponseEntity.ok(new SuccessResponse("Registration successful")); // Trả về thông báo thành công !
    }
}