package fjb.mock.mapper;

import fjb.mock.model.dto.parentDTO.ParentSchoolResponseDTO;
import fjb.mock.model.dto.schoolDTO.SchoolEnrollDetailDTO;
import fjb.mock.model.entities.SchoolEnrollDetail;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SchoolEnrollDetailMapper {
    SchoolEnrollDetailDTO toDTO(SchoolEnrollDetail schoolEnrollDetail);
    List<SchoolEnrollDetailDTO> toDTO(List<SchoolEnrollDetail> schoolEnrollDetails);
    List<ParentSchoolResponseDTO> toSchoolDTO(List<SchoolEnrollDetail> schoolEnrollDetails);
}
