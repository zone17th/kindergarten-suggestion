package fjb.mock.mapper;

import fjb.mock.model.dto.schoolDTO.adminSchoolDTO.AdminSchoolRequestDTO;
import fjb.mock.model.dto.schoolDTO.adminSchoolDTO.AdminSchoolResponseDTO;
import fjb.mock.model.dto.schoolDTO.adminSchoolDTO.SchoolListResponseDTO;
import fjb.mock.model.entities.School;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface AdminSchoolMapper {
    AdminSchoolResponseDTO toDTO(School school);
    School toEntity(AdminSchoolRequestDTO schoolDetailDTO);
    SchoolListResponseDTO toListDTO(School school);
}
