package fjb.mock.mapper;

import fjb.mock.model.dto.parentDTO.ParentDetailResponseDTO;
import fjb.mock.model.entities.FeedBack;
import fjb.mock.model.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ParentMapper {
    ParentDetailResponseDTO toDTO(User user);
}